# Deployment System Guide

=======================

## Contents

1. [Prerequisites](#prerequisites)
2. [File Locations](#file-locations)
3. [Developer Setup](#developer-setup)
4. [How Do I](#how-do-i)


=======================

get nemo5 Installer 
   
   npm install git+ssh://git@bitbucket.org/neofoniemobile/nemo5.git -g

## Prerequisites

- OSX Yosemite
- XCode 6.1 & XCode Commandline Tools (when building for iOS)
- Android SDK & Build Tools (when building for Android targets)

## File Locations

+ App asset templates (Photoshop)
   https://www.dropbox.com/sh/wnluurc94ael51z/AAA_FKieadzA_bA_8ieuw0UPa?dl=0

## Developer Setup

+ Update Ruby.
```sh
sudo gem update --system
```

+ Install SASS & Compass.
```sh
sudo gem install sass
sudo gem install compass
```

+ Install node & npm.   
   *[nodejs.org](https://www.nodejs.org)*

+ Install GruntJS.
```sh
npm install -g grunt-cli
```

+ Install Bower.
```sh
npm install -g bower
```

+ Install node dependencies (from package.json).
```sh
npm install
```

+ Install frontend dependencies (from bower.json).
```sh
bower install
```

+ Add Cordova platforms.
```sh
cordova platform add ios
cordova platform add android
```

+ Start local development server.
```sh
grunt serve
```


## How Do I

 + ...deploy to iOS *simulator*
    1. Choose devicetype
    ```shell
    ./platforms/ios/cordova/lib/list-emulator-images
    ```
    2. Run
    ```shell
    cordova emulate ios --target="iPhone-6-Plus"
    ```


 + ... (iOS) deploy to iOS *device*
    ```shell
    cordova run ios --device --release
    ```


 + ... (Android) deploy to *device+ or *emulator*
    ```shell
    cordova run android
    ```


 + ... bump the *application version*
    - Change the version in the following files:
        - /package.json (both *version+ & *_nemo.project.version+ JSON subtrees)
        - /bower.json
        - /config.xml


 + ... modify *code signing+ settings
    - Adapt settings in the package.json file, using this JSON object:
    - ```json
        ...
        _nemo: {
            deployment: {
                signing: {
                ...
                ```
    - The root folder for any file lookup in this regard is *_nemo/credentials/platform*.
    

 + .. *URL+ changes
    - Adapt settings in the **package.json*+ file, using the **_nemo.deployment.urls*+ JSON subtree.
    

 + *npm permission+ fixes:
    ```shell
    sudo chown -R $USER /usr/local/lib/node_modules
    sudo chown -R `whoami` ~/.npm
    sudo chown -R `whoami` /usr/local/lib
    ```

 + ... *increase verbosity+ of the commandline output:
    ```shell
    grunt nemo --verbose --stack-trace
    ```