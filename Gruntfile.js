'use strict';

/**
 * @summary Main Gruntfile
 * @copyright Neofonie Mobile GmbH
 * @author S. Bofah
 * @created 2015-01-02
 */

/** @external */
var path = require('path'),
    nemo = require('./lib/index.js');

module.exports = function(grunt) {

    // Provide runtime environment variables to Grunt
    grunt.ENV = {
        USER_HOME: process.env['_USER_HOME'],
        HOME_FILEPATH: process.env['_HOME_FILEPATH'],
        HOME_PACKAGE: process.env['_HOME_PACKAGE'],
        HOME_PACKAGE_FILEPATH: process.env['_HOME_PACKAGE_FILEPATH'],
        HOME_NAME: process.env['_HOME_NAME'],
        HOME_VERSION: process.env['_HOME_VERSION'],
        APP_FILEPATH: process.env['_APP_FILEPATH'],
        APP_PACKAGE: process.env['_APP_PACKAGE'],
        APP_PACKAGE_FILEPATH: process.env['_APP_PACKAGE_FILEPATH'],
        APP_NAME: process.env['_APP_NAME'],
        APP_VERSION: process.env['_APP_VERSION']
    };

    // Complete directory by prepending base path
    grunt.RESOURCE = {
        HOME: function(filePath) {
            return nemo.fs.prependPath(filePath, grunt.ENV.HOME_FILEPATH);
        },
        APP: function(filePath) {
            return nemo.fs.prependPath(filePath, grunt.ENV.APP_FILEPATH);
        }
    };
    grunt.GET = function(constant) {
        return grunt.config.get(['nemo', '_CONSTANT', constant]);
    };
    grunt.RESOLVE = {
        IOS: function(constant) {
            return grunt.config.get(['nemo', '_CONSTANT', 'ios', constant]);
        },
        ANDROID: function(constant) {
            return grunt.config.get(['nemo', '_CONSTANT', 'android', constant]);
        }
    };

    // Suppress Grunt Log Messages
    grunt.log.header = function() {};
    //grunt.fail.report = function() {};

    grunt.initConfig({

        pkg: nemo.fs.getJSON(grunt.ENV.APP_PACKAGE_FILEPATH),

        nemo: {
            '_CONSTANT': {
                'configuration': 'nemo5.json',
                'cli': 'cordova',
                'manifest': 'templates/manifest/schema.json',
                'output_dir': path.join(grunt.ENV.APP_FILEPATH, grunt.ENV.HOME_NAME, 'releases', grunt.ENV.APP_VERSION),
                'page_assets_dir': 'templates/page/assets',
                'page_index_file': 'templates/page/index.html',
                'page_icon_file': '<%= pkg.name %>.png',
                'resources_dir': path.join(grunt.ENV.APP_FILEPATH, 'resources'),
                'ios': {
                    application_file_name: '<%= pkg.name %>',
                    application_file_packaged: ['<%= pkg.name %>.ipa'],
                    application_file_unpackaged: ['<%= pkg.name %>.app'],
                    build_dir: 'platforms/ios/build/',
                    buildconfig_dir: 'platforms/ios/cordova/',
                    buildconfig_file_template: ['templates/build-configuration/build-release.xcconfig'],
                    buildconfig_file_extension: ['.xcconfig'],
                    downloadurl_file: '<%= pkg.name %>' + '.plist',
                    downloadurl_prefix: 'itms-services://?action=download-manifest&url=',
                    icon_file: 'platforms/ios/<%= pkg.name %>/Resources/icons/icon-60@3x.png',
                    platform_architecture: null,
                    platform_basename: 'ios',
                    platform_devicetypes: ['handset', 'tablet'],
                    platform_devicetypes_enum: { 'handset': '1', 'tablet': '2', 'universal': '1,2' },
                    platform_orientation: ['portrait', 'landscape'],
                    platform_dir: 'platforms/ios/',
                    platform_nicename: 'iOS',
                    profile_dir: '/Library/MobileDevice/Provisioning Profiles/',
                    resources_dir: 'platforms/ios/<%= pkg.name %>/Resources',
                    settings_file: 'platforms/ios/<%= pkg.name %>/<%= pkg.name %>' + '-Info.plist',
                    wireless_manifest_file: 'templates/wireless-manifest.plist'
                },
                'android': {
                    application_file_name: '<%= pkg.name %>',
                    application_file_packaged: ['<%= pkg.name %>.apk', '<%= pkg.name %>-x86.apk', '<%= pkg.name %>-armv7.apk'],
                    application_file_unpackaged: ['<%= pkg.name %>.apk', '<%= pkg.name %>-x86.apk', '<%= pkg.name %>-armv7.apk'],
                    build_dir: 'platforms/android/build/',
                    buildconfig_dir: 'platforms/android/',
                    buildconfig_file_template: ['templates/build-configuration/build-extras.gradle', 'templates/build-configuration/gradle.properties'],
                    buildconfig_file_extension: ['.gradle', '.properties'],
                    downloadurl_file: '<%= pkg.name %>' + '.apk',
                    downloadurl_prefix: '',
                    icon_file: 'platforms/android/res/drawable-xhdpi/icon.png',
                    platform_architecture: null,
                    platform_basename: 'android',
                    platform_devicetypes: ['handset', 'tablet'],
                    platform_devicetypes_enum: null,
                    platform_orientation: ['portrait', 'landscape'],
                    platform_dir: 'platforms/android/',
                    platform_nicename: 'Android',
                    profile_dir: null,
                    resources_dir: 'platforms/android/res',
                    settings_file: 'platforms/android/' + 'AndroidManifest.xml',
                    wireless_manifest_file: null
                }
            }
        }
    });

    // Load external tasks
    require('load-grunt-tasks')(grunt);


    // TODO: Move to Task level
    // grunt.task.run('notify_hooks');


    // Load Nemo5 Tasks
    grunt.task.loadTasks('tasks/internal-tasks');
    grunt.task.loadTasks('tasks');


    /** Help */
    grunt.registerTask('help', ['nemo-help']);

    /** Setup */
    grunt.registerTask('setup', ['nemo-setup']);

    /** Main Components */
    grunt.registerTask('app', ['nemo-setup', 'nemo-app']);
    grunt.registerTask('build', ['nemo-setup',  'nemo-app', 'nemo-build']);
    grunt.registerTask('page', ['nemo-setup', 'nemo-page']);
    grunt.registerTask('deploy', ['nemo-setup', 'nemo-deploy']);

    /** Sequence */
    grunt.registerTask('release', ['setup', 'app', 'build', 'page', 'deploy']);


    /** Default Task */
    grunt.registerTask('default', ['help']);



};
