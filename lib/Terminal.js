'use strict';

/* ==========================================================================
 * Terminal Utilities
 * ========================================================================== */

// Init
var Terminal = {};

/**
 * @description Clear text from terminal window
 */
Terminal.clear = function() {
    process.stdout.write('\u001B[2J\u001B[0;0f');
};

/**
 * @description Get terminal width
 * @returns {Number} Terminal columns
 */
Terminal.getWidth = function() {
    if (process.stdout.getWindowSize) {
        return process.stdout.getWindowSize()[0];
    } else {
        return 80;
    }
};

/**
 * @description Extend single text portion (one column) to full terminal width, respecting margins and padding
 * @param message {String} Text
 * @param wrapperCharacter {String=} Character to use for wrapping
 * @param marginWidth {Number=} Margin to each side of terminal borders
 * @param marginCharacter {String=} Character to use as margin
 * @returns {String} Formatted Text
 */
Terminal.wrapLine = function(message, wrapperCharacter, marginWidth, marginCharacter) {
    return Terminal.wrapColumns(message, null, wrapperCharacter, marginWidth, marginCharacter);
};

/**
 * @description Extend two text portions (two columns) to full terminal width, respecting margins and padding
 * @param messageLeft {String} Text (left column)
 * @param messageRight {String} Message (right column)
 * @param wrapperCharacter {String=} Character to use for wrapping
 * @param marginWidth {Number=} Margin to each side of terminal borders
 * @param marginCharacter {String=} Character to use as margin
 * @param fromRight {String=} Pad right column
 * @returns {String} Formatted Text
 */
Terminal.wrapColumns = function(messageLeft, messageRight, wrapperCharacter, marginWidth, marginCharacter, fromRight) {

    // Modules
    var stringLength = require('string-length'),
        _ = require('lodash');

    var padString = wrapperCharacter || ' ';

    var alignRight = Boolean(fromRight),
        alignLeft = Boolean(!alignRight);

    var leftText = messageLeft,
        leftSizeVisible = stringLength(leftText),
        leftSizeExtra = _.size(leftText) - leftSizeVisible,
        leftPad = padString;

    var rightText = messageRight || '',
        rightSizeVisible = stringLength(rightText),
        rightSizeExtra = _.size(rightText) - rightSizeVisible,
        rightPad = padString;

    var outputLeft,
        outputRight;

    var marginString = marginCharacter || ' ',
        marginSize = Number.isInteger(marginWidth) ? parseInt(marginWidth) : parseInt(2),
        marginComputed = marginSize === 0 ? '' : _.padLeft(marginString, marginSize);

    var screenWidth = Terminal.getWidth(),
        screenWidthActual = screenWidth - (marginSize + marginSize);

    if (alignLeft === true) {
        outputLeft = _.padRight(leftText, screenWidthActual + leftSizeExtra - rightSizeVisible, leftPad);
        outputRight = rightText;
    }

    if (alignLeft === false) {
        outputLeft = leftText;
        outputRight = _.padLeft(rightText, screenWidthActual + rightSizeExtra - leftSizeVisible, rightPad);
    }

    return marginComputed + outputLeft + outputRight + marginComputed;
};

/**
 * @description Extend single text portion (one column) to full terminal width, respecting padding
 * @param message {String} Message to wrap
 * @param wrapperCharacter {String=} Character to use for wrapping
 * @returns {String} Formatted Text
 */
Terminal.wrapLineFull = function(message, wrapperCharacter) {
    return Terminal.wrapLine(message, wrapperCharacter, 0);
};

/**
 * @description Extend two text portions (two columns) to full terminal width, respecting padding
 * @param messageLeft {String} Message, left column
 * @param messageRight {String} Message, right column
 * @param wrapperCharacter {String=} Character to use for wrapping
 * @returns {String} Formatted Text
 */
Terminal.wrapColumnsFull = function(messageLeft, messageRight, wrapperCharacter) {
    return Terminal.wrapColumns(messageLeft, messageRight, wrapperCharacter, 0);
};


/**
 * @description Print readable JSON
 * @param jsObject {String|Object} Javascript Object
 * @param objectDescription {String=} Description / Name / Source Filename of the object
 * @return {String} Configuration
 */
Terminal.printObject = function(jsObject, objectDescription) {

    /** @external */
    var br = require('os').EOL,
        chalk = require('chalk'),
        eyes = require('eyes');

    // Rendering options
    var objectName = objectDescription || 'JSON',
        renderOptions = {
            styles: {
                all: 'cyan',
                label: 'underline',
                other: 'inverted',
                key: 'bold',
                special: 'grey',
                string: 'green',
                number: 'magenta',
                bool: 'blue',
                regexp: 'green'
            },
            pretty: true,
            hideFunctions: false,
            stream: null
        };

    process.stdout.write(br);
    process.stdout.write(chalk.styles.dim.open + chalk.styles.cyan.open + Terminal.wrapLine(objectName + ' ', '─') + chalk.styles.cyan.close + chalk.styles.dim.close);

    process.stdout.write(br);
    process.stdout.write(Terminal.wrapLine(eyes.inspector(renderOptions)(jsObject)));

    process.stdout.write(br);
    process.stdout.write(chalk.styles.dim.open + chalk.styles.cyan.open + Terminal.wrapLine('─────', '─') + chalk.styles.cyan.close + chalk.styles.dim.close);
};


/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = Terminal;
