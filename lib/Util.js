'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Utility Functions
 * ========================================================================== */

// Init
var Util = {};

/**
 * @description Run shell scripts synchronously and prints the result to the terminal
 * @param commandName {String} Name of shell command (full path is automatically added via 'npm-which'
 * @param commandArgs {Array=} List of shell command arguments as array
 * @returns {Boolean} Result of Shell execution
 */
Util.runShell = function(commandName, commandArgs) {

    /**
     * @constant
     */
    var _TITLE = 'Shell Runner';

    /** @external */
    var shellSpawn = require('child_process').spawnSync;

    // Init
    var command = commandName,
        childProcess = shellSpawn(command, commandArgs, { stdio: 'inherit' });

    if (childProcess.error) {
        nemo.logger.error('Error running command.', '"' + commandName + ' ' + commandArgs.join(' ') + '"', _TITLE);
        nemo.logger.errorData(childProcess.stdout);
        nemo.logger.errorData(childProcess.stderr);
        return false;
    }

    nemo.logger.log('Running command.', '"' + commandName + ' ' + commandArgs.join(' ') + '"', _TITLE);
    nemo.logger.logData(childProcess.stdout);
    return true;
};

/**
 * @description Run shell scripts and returns true or false depending on their exit code.
 * @param commandName {String} Name of shell command (full path is automatically added via 'npm-which'
 * @param commandArgs {Array=} List of shell command arguments as array
 * @param shellEncoding {String=}
 * @param pipeIn {*=} Pipe input to command
 * @returns {*} Result of Shell execution
 */
Util.getShell = function(commandName, commandArgs, shellEncoding, pipeIn) {

    /** @external */
    var shellSpawn = require('child_process').spawnSync;

    /** @defaults */
    var shellOptions = {
        input: pipeIn,
        encoding: shellEncoding
    };

    // Init
    var command = commandName,
        childProcessResult = shellSpawn(command, commandArgs, Util.compact(shellOptions));

    // Result: Error
    if (childProcessResult.status === 0 && childProcessResult.stdout) {
        return childProcessResult.stdout;
    }

    // Result: OK
    return false;
};

/**
 * @description Returns duplicate-free array that contains all supplied arrays' elements
 * @args var_args {...Array}
 * @returns {Array}
 */
Util.syncLists = function() {

    /** @external */
    var _ = require('lodash');

    var args = Array.prototype.slice.call(arguments),
        argsMerged = [];

    _.each(args, function(item) {
        argsMerged = argsMerged.concat(item);
    });

    return _.uniq(argsMerged);
};

/**
 * @description Remove all empty elements from passed arrays or objects
 * @param target {Array|Object} Array or Object
 * @returns {*}
 */
Util.compact = function(target) {

    /** @external */
    var _ = require('lodash');

    // Init
    var targetComplete = target,
        targetCompact,
        compactRecursive = function(obj) {
            return _.transform(obj, function(o, v, k) {
                if (v && _.isPlainObject(v)) {
                    o[k] = compactRecursive(v);
                } else if (v) {
                    o[k] = v;
                }
            });
        };

    if (_.isPlainObject(targetComplete)) {
        targetCompact = compactRecursive(targetComplete);
    }

    // Array
    if (_.isArray(targetComplete)) {
        targetCompact = _.compact(targetComplete);
    }

    return targetCompact;
};

/**
 * @description Tests if variables do not contain any information
 * @param val {*} Any object
 * @returns {Boolean}
 */
Util.isEmpty = function(val) {
    var _ = require('lodash');

    return !!(_.isNull(val) || _.isUndefined(val) || _.isEmpty(val));


};


/**
 * @description Tests if variables do contain any information
 * @param val {*} Any object
 * @returns {Boolean}
 */
Util.notEmpty = function(val) {
    return !Util.isEmpty(val);
};


/**
 * @description Converts String to Array by placing it at index 0.
 * @param val {String} String
 * @returns {Array}
 */
Util.toArray = function(val) {
    return [val];
};

/**
 * @description Recursively find object values by Key
 * @param targetObject {Object}
 * @param keyName {String}
 * @returns {Array}
 */
Util.findValuesByKey = function(targetObject, keyName) {

    /** @external */
    var _ = require('lodash');

    var findRecursive = function(object, list) {

        var obj = object,
            results = list || [];

        if (_.isPlainObject(obj)) {
            for (keyName in obj) {
                if (obj.hasOwnProperty(keyName)) {
                    results.push(obj[keyName]);
                }
            }
        } else {
            //not an Object so obj[k] here is a value
            results.push(obj[keyName]);
        }

        return results;
    };

    return findRecursive(targetObject);
};

/**
 * @description Makes complex strings URL-compatible by converting them to lowercase and replacing spaces with hyphens.
 * @param name {String} String
 * @returns {String}
 */
Util.hyphenate = function(name) {
    return name.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};

/**
 * @description Converts Array to String by picking the item at index 0.
 * @param val {Array} String
 * @returns {Array}
 */
Util.pickArray = function(val) {

    var _ = require('lodash');

    if (_.isArray(val)) {
        return Util.pickArray(val[0]);
    }
    return val;
};

/**
 * @description Returns a nice formatted depiction of an absolute path
 * @param pathName {String} - absolute path
 * @param pathDepth {String=} - how many parent directories should be shown
 * @param pathPrefix {String=} - prefix
 * @returns {String} pretty path
 */
Util.beautifyPath = function(pathName, pathDepth, pathPrefix) {

    /** @external */
    var path = require('path');

    var name = pathName || process.cwd(),
        prefix = pathPrefix || '...';

    return path.join(prefix, path.basename(path.dirname(name)), path.basename(name));
};


/**
 * @description Turn Array to Keypath
 * @args var_args {...Array} Array
 * @returns {String} Keypath
 */
Util.getKeypath = function() {

    /** @external */
    var _ = require('lodash');

    var args = Array.prototype.slice.call(arguments),
        argsCompact = _.compact(args);

    return argsCompact.join('.');
};




/**
 * @description Convert JSON data structures to JavaScript objects.
 * @param jsonData {*} Plist data
 * @return {Object|Boolean} JS Object
 */
Util.parseJson = function(jsonData) {

    // Init
    var data = jsonData,
        jsonContent;

    // Parse plist data
    try {
        jsonContent = JSON.parse(data);
    } catch (error) {
        nemo.logger.errorData(error);
        return false;
    }

    return jsonContent;
};

/**
 * @description Convert .plist data structures to JavaScript objects.
 * @param plistData {*} Plist data
 * @return {Object|Boolean} JS Object
 */
Util.parsePlist = function(plistData) {

    /** @external */
    var plist = require('simple-plist');

    // Init
    var data = plistData,
        plistContent;

    // Parse plist data
    try {
        plistContent = plist.parse(data);
    } catch (error) {
        return false;
    }

    return plistContent;
};

/**
 * @description Convert XML data structures to JavaScript objects
 * @param xmlData {*} XML data
 * @return {Object|Boolean} JS Object
 */
Util.parseXml = function(xmlData) {

    /** @external */
    var xml = require('xml-mapping');

    // Init
    var data = xmlData,
        content;

    // Parse XML data
    try {
        content = xml.load(data);
    } catch (error) {
        nemo.logger.errorData(error);
        return false;
    }

    return content;
};

/**
 * @description Convert x509-formatted certificate data to JavaScript objects
 * @param certificateData {*} base64 encoded PEM string
 * @return {Object|Boolean} JS Object
 */
Util.parseCertificate = function(certificateData) {

    /** @external **/
    var x509 = require('x509');

    // Init
    var data = certificateData,
        content;

    // Parse P12 data
    try {
        content = x509.parseCert(data);
    } catch (error) {
        nemo.logger.errorData(error);
        return false;
    }

    return content;
};

/**
 * @description Parse Mobile Device Provisioning Profiles (wrapper for .plist parser)
 * @param profileData {*} base64 encoded PEM string
 * @return {Object|Boolean} JS Object
 */
Util.parseProfile = function(profileData) {
    return Util.parsePlist(profileData);
};

/**
 * @description Convert Keystore to JavaScript objects
 * @param keystoreData {*} Keystore string
 * @return {Object|Boolean}
 */
Util.parseKeystore = function(keystoreData) {

    /** @default */
    var data = keystoreData;

    if (!data) {
        return false;
    }

    return {
        alias: data.match(/(Alias name:.*?(.*))\w+/g)[0].split(': ')[1]
    };
};

/**
 * @description Get SHA-1 fingerprint from x509 certificate data
 * @param x509Data {*} x509 string
 * @return {Object|Boolean} JS Object
 */
Util.hashCertificate = function(x509Data) {

    /** @defaults **/
    var sslCommand = nemo.fs.lookupExecutable('openssl');

    /** @constant **/
    var data = x509Data,
        args = ['x509', '-noout', '-fingerprint'],
        hash = nemo.util.getShell(sslCommand, args, 'utf8', data),
        hashResult;

    // Remove all special characters from fingerprint
    hashResult = hash.replace(/:/g, '');
    hashResult = hashResult.substring(hashResult.indexOf('=') + 1).trim();

    return hashResult;
};



/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = Util;
