'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Git nemo.utilities
 * ========================================================================== */

// Init
var Git = {};

/**
 * @description Gets list of local git branches in directory
 * @param repoPath {String} Path to Git Repo
 * @returns {Array|Boolean} List of local branches
 */
Git.listBranches = function(repoPath) {

    /** @defaults **/
    var gitCommandName = 'git',
        formatCommandName = 'cut';

    // Filesystem
    var filePath = repoPath;

    // Commands
    var gitCommandArguments = ['-C', filePath, 'branch'],
        formatCommandArguments = ['-c', '3-'],
        formatCommandOptions = { 'input': nemo.util.getShell(gitCommandName, gitCommandArguments) },
        branchesList = nemo.util.compact(nemo.util.getShell(formatCommandName, formatCommandArguments, formatCommandOptions)),
        branchesResult;

    if (branchesList) {
        branchesResult = branchesList.split(/\n/);
        return branchesResult;
    }

    return false;
};

/**
 * @description Gets active Git Branches
 * @param repoPath {String} Path to Git Repo
 * @returns {String|Boolean} Get currently active branch or false if git has not been set up.
 */
Git.getActiveBranch = function(repoPath) {

    /** @external */
    var _ = require('lodash');

    /** @defaults **/
    var gitCommandName = 'git',
        formatCommandName = 'sed';

    // Filesystem
    var filePath = repoPath;

    // Commands
    var gitCommandArguments = ['-C', filePath, 'branch'],
        formatCommandArguments = ['-n', '/\* /s///p'],
        formatCommandOptions = { 'input': nemo.util.getShell(gitCommandName, gitCommandArguments) },
        branchName;

    // Get current branch
    branchName = _.trim(nemo.util.getShell(formatCommandName, formatCommandArguments, formatCommandOptions));

    if (!branchName) {
        return false;
    }

    return branchName;
};

/**
 * @description Checks whether Git has been initialized in a directory
 * @param repoPath {String} Path to Git Repo
 * @returns {Boolean} Is Git installed in this directory?
 */
Git.isInitialized = function(repoPath) {

    /** @external */
    var _ = require('lodash');

    /** @defaults **/
    var gitCommandName = 'git';

    // Filesystem
    var filePath = repoPath;

    // Commands
    var gitCommandArguments = ['-C', filePath, 'rev-parse', '--is-inside-work-tree'],
        isGit = _.trim(nemo.util.getShell(gitCommandName, gitCommandArguments));

    return Boolean(isGit);
};



/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = Git;
