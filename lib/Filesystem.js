'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Filesystem
 * ========================================================================== */

// Init
var Filesystem = {};

/**
 * @description Normalize directory names.
 * @param filePath {String} String with path
 * @returns {String} Normalized Path
 */
Filesystem.normalizePath = function(filePath) {
    return Filesystem.normalize(filePath, Boolean(true));
};

/**
 * @description Normalize Filesystem locations. Adds trailing slash to paths if not present.
 * @param filePath {String} String with path
 * @param addSeparator {Boolean=} If true, will
 * @returns {String} Normalized Path
 */
Filesystem.normalize = function(filePath, addSeparator) {

    /** @external */
    var path = require('path'),
        _ = require('lodash');

    // Init
    var pathName = filePath,
        pathSeparator = path.sep,
        forceTrailingSep = addSeparator || false,
        pathNormalized;

    // 1. Normalize via nodes path()
    pathNormalized = path.normalize(pathName);

    // 2. Add trailing slash
    if (forceTrailingSep === true) {
        if (!_.endsWith(pathNormalized, pathSeparator)) {
            pathNormalized = pathNormalized.concat(pathSeparator);
        }
    }

    return pathNormalized;
};

/**
 * @description Prepend Base Path to a file or an array of files
 * @param filePath {Array,String} Directory or list of directories to which a string should be prepended
 * @param basePath {String} Path string to prepend
 * @returns {Array|String}
 */
Filesystem.prependPath = function(filePath, basePath) {

    /** @external */
    var _ = require('lodash'),
        path = require('path');

    // Init
    var pathNames = filePath,
        base = Filesystem.normalizePath(basePath),
        pathNamesCompleted;

    // Single file
    if (_.isString(pathNames)) {
        pathNamesCompleted = path.join(base, pathNames);
    }

    // List of files
    if (_.isArray(pathNames)) {
        pathNamesCompleted = [];
        _.each(pathNames, function(item) {
            pathNamesCompleted.push(path.join(base, item));
        });
    }

    return pathNamesCompleted;
};

/**
 * @description Lookup executable using "which"
 * @param title {String} Binary Name, e.g. "xcrun", "cordova", "ionic"
 */
Filesystem.lookupExecutable = function(title) {

    /** @external */
    var path = require('path'),
        which = require('npm-which');

    // Init
    var executableName = path.basename(title),
        executableResult;

    try {
        executableResult = which.sync(executableName, { cwd: process.cwd() });
    } catch (error) {
        return false;
    }

    return executableResult;
};

/**
 * @description List Filesystem items
 * @param filePath {String} Base lookup Directory
 * @param showDeep {Boolean=} Recurse subdirectories
 * @param showFolders {Boolean=} Recurse subdirectories
 * @param filterList {Array=} Filter files by extension or folders by name
 * @returns {Array} List of files
 */
Filesystem.getList = function(filePath, showDeep, showFolders, filterList, showPath) {

    /** @external */
    var globule = require('globule'),
        _ = require('lodash'),
        path = require('path');

    /** @constants */
    var patternFilterTypeFile = 'isFile',
        patternFilterTypeFolder = 'isDirectory',
        patternPrefixScopeDeep = '**',
        patternPrefixScopeFlat = '',
        patternSuffixScopeDeep = '',
        patternSuffixScopeFlat = '',
        patternPrefixTypeFile = '*.',
        patternPrefixTypeFolder = '';

    // Init
    var folder = Filesystem.normalize(filePath),
        filter = _.map(filterList, function(v) { return _.trim(v, '.');});

    // Glob Pattern
    var prefixFullPath = showPath === true,
        scopePrefix = showDeep === true ? patternPrefixScopeDeep : patternPrefixScopeFlat,
        scopeSuffix = showDeep === true ? patternSuffixScopeDeep : patternSuffixScopeFlat,
        typePrefix = (showFolders === true) ? patternPrefixTypeFolder : patternPrefixTypeFile,
        typeFilter = (showFolders === true) ? patternFilterTypeFolder : patternFilterTypeFile,
        whitelist = _.isArray(filter) && nemo.util.notEmpty(filter) ? '{,' + filter.join(',') + '}' : '*',
        pattern = path.join(scopePrefix, typePrefix + whitelist, scopeSuffix),
        results;

    // Init
    results = globule.find({
        filter: typeFilter,
        srcBase: folder,
        prefixBase: prefixFullPath,
        src: pattern
    });

    return results;
};

/**
 * @description List Files
 * @param filePath {String} Base lookup Directory
 * @param extensionList {Array=} Allowed file extensions (.jpg, .png)
 * @param prefixFolder {Boolean=} Add path prefix
 * @returns {Array} List of files
 */
Filesystem.getFiles = function(filePath, extensionList, prefixFolder) {
    return Filesystem.getList(filePath, false, false, extensionList, prefixFolder);
};

/**
 * @description List Files recursively
 * @param filePath {String} Base lookup Directory
 * @param extensionList {Array=} Allowed file extensions, e.g. ['.jpg', '.png']
 * @param prefixFolder {Boolean=} Add path prefix
 * @returns {Array} List of files
 */
Filesystem.getFilesDeep = function(filePath, extensionList, prefixFolder) {
    return Filesystem.getList(filePath, true, false, extensionList, prefixFolder);
};

/**
 * @description List Folders
 * @param filePath {String} Base lookup Directory
 * @param folderNameList {Array=} Allowed folder names, e.g. ['folder1', 'folder2']
 * @param prefixFolder {Boolean=} Add path prefix
 * @returns {Array} List of folders
 */
Filesystem.getFolders = function(filePath, folderNameList, prefixFolder) {
    return Filesystem.getList(filePath, false, true, folderNameList, prefixFolder);
};

/**
 * @description List Folders recursively
 * @param filePath {String} Base lookup Directory
 * @param folderNameList {Array=} Allowed folder names, e.g. ['folder1', 'folder2']
 * @param prefixFolder {Boolean=} Add path prefix
 * @returns {Array} List of folders
 */
Filesystem.getFoldersDeep = function(filePath, folderNameList, prefixFolder) {
    return Filesystem.getList(filePath, true, true, folderNameList, prefixFolder);
};

/**
 * @description Check if Path exists
 * @param  filePath {String} Path
 * @returns {Object, Boolean}
 */
Filesystem.isExisting = function(filePath) {

    /** @external */
    var fs = require('fs');

    // Init
    var file = Filesystem.normalize(filePath),
        fileResult;

    try {
        fileResult = fs.lstatSync(file);
    } catch (error) {
        //nemo.logger.error(error, file)
        return false;
    }

    return fileResult;
};

/**
 * @description Check if Path is a directory
 * @param  filePath {String} Path
 * @returns {String, Boolean}
 */
Filesystem.isDirectory = function(filePath) {

    // Init
    var location = filePath;

    // Check if location exists
    if (Filesystem.isExisting(location)) {
        if (Filesystem.isExisting(location).isDirectory()) {
            return location;
        }
    }

    return false;
};

/**
 * @description Check if Path is a file
 * @param  filePath {String} Path
 * @returns {String, Boolean}
 */
Filesystem.isFile = function(filePath) {

    // Init
    var location = filePath;

    // Check if location exists
    if (Filesystem.isExisting(location)) {
        if (Filesystem.isExisting(location).isFile()) {
            return location;
        }
    }

    return false;
};

/**
 * @description Write data to file
 * @param filePath {String} location
 * @param fileData {*} data
 * @param writeEncoding {String=}
 * @returns {Boolean}
 */
Filesystem.writeFile = function(filePath, fileData, writeEncoding) {

    /** @external */
    var fs = require('fs');

    /** @defaults */
    var writeOptions = {
        encoding: writeEncoding
    };

    // Init
    var file = Filesystem.normalize(filePath),
        data = fileData,
        writeResult;

    // Write data to file
    try {
        writeResult = fs.writeFileSync(file, data, writeOptions);
    }
    catch (error) {
        //nemo.logger.error(error, file);
        return false;
    }

    return writeResult;
};

/**
 * @description Create Directory
 * @param filePath {String} location
 * @returns {Boolean}
 */
Filesystem.createPath = function(filePath) {

    /** @external */
    var fs = require('fs');

    // Init
    var pathName = Filesystem.normalizePath(filePath);

    // Create path
    try {
        fs.mkdirSync(pathName);
    }
    catch (error) {
        //nemo.logger.error(error, pathName);
        return false;
    }

    return true;
};

/**
 * @description Remove all folders and files inside a folder, but leave folder itself intact
 * @param filePath {String} Path to directory
 * @returns {Array|Boolean}
 */
Filesystem.deletePathContent = function(filePath) {

    /** @external */
    var del = require('del');

    /** @default */
    var cleanOptions = {
        force: true
    };

    // Init
    var pathName = Filesystem.normalizePath(filePath),
        targetPath = Filesystem.isDirectory(pathName),
        targetPattern = [targetPath + '/*', targetPath + '/*/*'],
        cleanResult;

    // Remove folders and files from directory
    cleanResult = del.sync(targetPattern, cleanOptions);

    // Print result
    if (nemo.util.isEmpty(cleanResult)) {
        nemo.logger.log('No files deleted in directory.', nemo.util.beautifyPath(targetPath));
        return false;
    }

    return cleanResult;
};

/**
 * @description Retrieve file content
 * @param  filePath {String} Path
 * @param  readEncoding {String=} Options
 * @returns {String, Boolean}
 */
Filesystem.readFile = function(filePath, readEncoding) {

    /** @external */
    var fs = require('fs');

    /** @defaults */
    var readOptions = {
        encoding: readEncoding || null
    };

    // Init
    var file = Filesystem.isFile(filePath),
        data;

    // Read file
    try {
        data = fs.readFileSync(file, readOptions);
    } catch (error) {
        //nemo.logger.error(error, file);
        return false;
    }

    return data;
};




/**
 * @description Return JavaScript objects from .json files (without having to use nested try/catch blocks).
 * @param filePath {String} Path to .plist
 * @return {Object|Boolean}
 */
Filesystem.getJSON = function(filePath) {

    var file = Filesystem.normalize(filePath),
        data = Filesystem.readFile(file, 'utf8'),
        jsonContent;

    jsonContent = nemo.util.parseJson(data);

    return jsonContent;
};

/**
 * @description Encode Javascript objects as JSON and write to file
 * @param filePath {String} Target path
 * @param object {String} Javascript Object
 * @returns {Boolean} Write result
 */
Filesystem.writeJSON = function(filePath, object) {

    // Init
    var data = object,
        file = Filesystem.normalize(filePath),
        jsonContent = JSON.stringify(data, null, 4),
        writeResult;

    // Write file
    writeResult = Filesystem.writeFile(file, jsonContent, 'utf8');

    return writeResult;
};

/**
 * @description Return JavaScript objects from .plist files (wrapper).
 * @param filePath {String} Path to .plist
 * @return {Object|Boolean}
 */
Filesystem.getPlist = function(filePath) {

    var file = Filesystem.normalize(filePath),
        data = Filesystem.readFile(file, 'utf8'),
        plistContent = nemo.util.parsePlist(data);

    return plistContent;
};

/**
 * @description Encode Javascript objects as plist and write to file
 * @param filePath {String} Target path
 * @param object {Object} Javascript Object
 * @returns {Boolean} Write result
 */
Filesystem.writePlist = function(filePath, object) {

    /** @external */
    var plist = require('simple-plist');

    // Init
    var data = object,
        file = Filesystem.normalize(filePath),
        writeResult;

    // Write file
    writeResult = plist.writeFileSync(file, data);

    return writeResult;
};

/**
 * @description Return JavaScript objects from .xml files (wrapper).
 * @param filePath {String} Path to .plist
 * @return {Object|Boolean}
 */
Filesystem.getXML = function(filePath) {

    var file = Filesystem.normalize(filePath),
        data = Filesystem.readFile(file),
        xmlContent = nemo.util.parseXml(data);

    return xmlContent;
};

/**
 * @description Encode Javascript objects as XML and write to file
 * @param filePath {String} Target path
 * @param object {Object} Javascript Object
 * @returns {Boolean} Write result
 */
Filesystem.writeXML = function(filePath, object) {

    /** @external */
    var xml = require('xml-mapping');

    // Init
    var data = object,
        file = Filesystem.normalize(filePath),
        xmlContent = xml.dump(data),
        writeResult;

    // Write file
    writeResult = Filesystem.writeFile(file, xmlContent);

    return writeResult;
};


/**
 * @description Return JavaScript objects from .xml files (wrapper).
 * @param filePath {String} Path to .plist
 * @return {Object|Boolean}
 */
Filesystem.getXML = function(filePath) {

    var file = Filesystem.normalize(filePath),
        data = Filesystem.readFile(file),
        xmlContent = nemo.util.parseXml(data);

    return xmlContent;
};

/**
 * @description Read SSL certificates & key files
 * @param filePath {String} Path to certificate
 * @param sslPassword {String}
 * @return {Object|Boolean}
 */
Filesystem.readSSL = function(filePath, sslPassword) {

    /** @external **/
    var path = require('path');

    /** @defaults **/
    var sslCommand = Filesystem.lookupExecutable('openssl');

    /** @constant **/
    var file = Filesystem.isFile(filePath),
        format = path.extname(file) || '.p12',
        password = sslPassword,
        formatMap = {
            '.p12': ['pkcs12', '-in', file, '-passin', 'pass:' + password, '-nodes'],
            '.cer': ['pkcs12', '-in', file, '-passin', 'pass:' + password, '-inform', 'der', '-subject', '-issuer']
        },
        sslData;

    // Return SSL certificate content
    sslData = nemo.util.getShell(sslCommand, formatMap[format], 'utf8');

    return sslData;
};

/**
 * @description Return JavaScript objects from .p12 and .cer certificates (wrapper).
 * @param filePath {String} Path to certificate
 * @param certificatePassword {String}
 * @return {Object|Boolean}
 */
Filesystem.getCertificate = function(filePath, certificatePassword) {

    var file = Filesystem.isFile(filePath),
        password = certificatePassword,
        data = Filesystem.readSSL(file, password),
        certificateContent = nemo.util.parseCertificate(data);

    return certificateContent;
};

/**
 * @description Read Apple provisioning profile data. Although these are plist files, CMS decryption is required before parsing.
 * @param filePath {String} Path to .mobileprovision
 * @return {Object|Boolean}
 */
Filesystem.readProfile = function(filePath) {

    /** @defaults **/
    var profileCommand = Filesystem.lookupExecutable('security');

    // Filesystem
    var file = Filesystem.isFile(filePath);

    // Init
    var args = ['cms', '-D', '-i', file],
        profileData = nemo.util.getShell(profileCommand, args, 'utf8');

    return profileData;
};

/**
 * @description Return JavaScript objects from Mobile provisioning profile files.
 * @param filePath {String} Path to .plist
 * @return {Object|Boolean}
 */
Filesystem.getProfile = function(filePath) {

    var file = Filesystem.normalize(filePath),
        data = Filesystem.readProfile(file),
        profileContent = nemo.util.parseProfile(data);

    return profileContent;
};


/**
 * @description Read Keystore certificates
 * @param filePath {String} Path to certificate
 * @param keystorePassword {String}
 * @return {Object|Boolean}
 */
Filesystem.readKeystore = function(filePath, keystorePassword) {

    /** @defaults **/
    var keystoreCommand = Filesystem.lookupExecutable('keytool');

    // Init
    var file = Filesystem.isFile(filePath),
        password = keystorePassword,
        args = ['-list', '-rfc', '-keystore', file, '-storepass', password],
        keystoreData;

    // Return SSL certificate content
    keystoreData = nemo.util.getShell(keystoreCommand, args, 'utf8');

    return keystoreData;
};

/**
 * @description Return JavaScript objects from Keystore files.
 * @param filePath {String} Path to .keystore
 * @param keystorePassword {String}
 * @return {Object|Boolean}
 */
Filesystem.getKeystore = function(filePath, keystorePassword) {

    var file = Filesystem.isFile(filePath),
        password = keystorePassword,
        data = Filesystem.readKeystore(file, password),
        keystoreContent = nemo.util.parseKeystore(data);

    return keystoreContent;
};


/**
 * @description Install P12 certificate
 * @param filePath {String} path to iOS .mobileprovision
 * @param password {String} Password for p12 file
 * @returns {Object|Boolean}
 */
//Filesystem.installMobileprovision = function(filePath, password) {
//
//
//};


/**
 * @description Check a array list of files for existance in the filesystem
 * @param fileList {Array}
 * @returns {Object}
 */
Filesystem.validateFileList = function(fileList) {

    /** @external */
    var _ = require('lodash');

    // Init
    var searchFileList = fileList,
        searchResult = {
            lost: [],
            found: [],
            error: false
        };

    // Iterate
    _.each(searchFileList, function(item) {
        if (Filesystem.isExisting(item)) {
            searchResult['found'].push(item);
        } else {
            searchResult['lost'].push(item);
            searchResult['error'] = true;
        }
    });

    return searchResult;
};


/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = Filesystem;
