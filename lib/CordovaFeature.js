'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Cordova Features
 * ========================================================================== */

// Init
var CordovaFeature = {};

/**
 * @description Batch-execute a cordova command for a certain feature (plugins/platforms), using a single item or a list of items.
 * @param filePath {String} Path to Cordova project
 * @param featureType {String} plugin, platform
 * @param cordovaAction {String} add, remove
 * @param featureItems {String|Array} feature identifier (e.g. 'org.apache.cordova.device')
 */
CordovaFeature.manage = function(filePath, featureType, cordovaAction, featureItems) {

    /** @external */
    var _ = require('lodash');


    /** @constant */
    var commandName = 'cordova',
        commandFlags = ['--save'],
        cordovaDictionary = {
            add: {
                'progressive': 'Installing',
                'noun': 'Installation'
            },
            remove: {
               'progressive': 'Removing',
                'noun': 'Removal'
            }
        };


    /** Normalize feature names (singular) */
    featureType = _.trimRight(featureType, 's');

    // Init
    var projectPath = nemo.fs.normalize(filePath),
        featureitemQueue = [],
        featureitemInstalled = CordovaFeature.getFeatureInstalled(projectPath, featureType),
        featureitemSaved = CordovaFeature.getFeatureSaved(projectPath, featureType);


    /**
     * Two Cordova feature configuration array formats are recognized:
     * complex  -   id@version: target and
     *              target: id@version
     * simple   -   id@version
     * @example
     *      "plugins": [ "org.apache.cordova.console@1.0", "com.ionic.keyboard" ]
     * @example
     *      "platforms": [ { "ios": "7.1" }, { "android@4.0.0-dev": "14" } ],
     */
    // 1. 'Complex', array-of-objects
    if (_.isPlainObject(featureItems)) {
        _.map(featureItems, function(v, k) {
            var feature = k, target = v;
            if (target.indexOf('@') !== -1) {
                feature = v;
                target = k;
            }
            featureitemQueue.push({
                identifier: feature.split('@')[0],
                version: feature.split('@')[1] || null,
                target: target
            });
        });
    }
    // 2. 'Simple', array-of-strings
    if (_.isArray(featureItems)) {
        _.each(featureItems, function(item) {
            featureitemQueue.push({
                identifier: item.split('@')[0],
                version: item.split('@')[1] || null
            });
        });
    }

    /**  Handling of batch feature selection via asterisk character (*)  */
    if (cordovaAction === 'remove') {
        if (_.isString(featureItems) && (featureItems === '*')) {
            _.each(featureitemInstalled, function(item) {
                featureitemQueue.push({
                    identifier: item.split('@')[0]
                });
            });
            _.each(featureitemSaved, function(item) {
                featureitemQueue.push({
                    identifier: item.name
                });
            });
        }
    }

    /** Use Git when installing platforms by version */
    if (featureType === 'platform') {
        _.each(featureitemQueue, function(item) {
            if (item.version) {
                commandFlags.push('--usegit');
            }
        });
    }

    /** Abort if feature  */
    if (featureitemQueue.length === 0) {
        return;
    }

    _.each(featureitemQueue, function(item) {
        var featureIdentifier = item.identifier,
            featureVersion = item.version,
            commandArgs;

        /** If a version is provided, add it to the identifier **/
        if (featureVersion) {
            featureIdentifier = featureIdentifier + '@' + featureVersion;
        }

        /** Assemble the actual CLI command and its readable version */
        commandArgs = [featureType, cordovaAction].concat(featureIdentifier).concat(commandFlags);

        /** Run shell script **/
        //nemo.util.runShell(commandName, commandArguments);
        nemo.logger.log(cordovaDictionary[cordovaAction]['progressive'] + ' ' + _.startCase(featureType) + ': ' + featureIdentifier, commandName + ' ' + commandArgs.join(' '));
        nemo.util.getShell(commandName, commandArgs);

    });

    nemo.logger.logData(cordovaDictionary[cordovaAction]['noun'] + ' ' + 'of' + ' ' + _.startCase(featureType) + 's' + ' ' + 'complete.');

};


/**
 * @description Retrieves all installed (to filesystem) instances of a given feature at given Cordova project path.
 * @param filePath {String} Path to Cordova Project
 * @param featureType {'platform'|'plugin'} Feature type
 * @returns {Array|Boolean}
 */
CordovaFeature.getFeatureInstalled = function(filePath, featureType) {
    if (featureType === 'platform') {
        return nemo.cordovainfo.getPlatformsInstalled(filePath);
    }
    if (featureType === 'plugin') {
        return nemo.cordovainfo.getPluginsInstalled(filePath);
    }
};

/**
 * @description Retrieves all saved (to configuration file) instances of a given feature at given Cordova project path.
 * @param filePath {String} Path to Cordova Project
 * @param featureType {String} Feature type name
 * @returns {Object|Boolean}
 */
CordovaFeature.getFeatureSaved = function(filePath, featureType) {
    if (featureType === 'platform') {
        return nemo.cordovainfo.getPlatformsSaved(filePath);
    }
    if (featureType === 'plugin') {
        return nemo.cordovainfo.getPluginsSaved(filePath);
    }
};

/**
 * Lookup the true identifier of a feature (for features installed via means other than Plugman)
 * @param filePath {String} Path to Cordova Project
 * @param featureType {String} Feature type name
 * @param currentId {String} Current feature id
 * @returns {*}
 */
CordovaFeature.getFeatureId = function(filePath, featureType, currentId) {

    /** @external */
    var path = require('path'),
        _ = require('lodash');

    /** @constant */
    var configPlugin = 'plugin.xml';

    // Init
    var projectPath = filePath,
        featureId = currentId,
        featureConfigContent;

    // Approximate actual id
    if (((_.startsWith(featureType, 'plugin')) && (_.startsWith(featureId, 'http') || _.startsWith(featureId, 'git') || _.startsWith(featureId, '/')))) {
        // 1. Replace current identifier with path basename (last portion of url-path)
        featureId = path.basename(featureId);
        // 2. Try to read plugin.xml
        featureConfigContent = nemo.util.parseXml(nemo.fs.readFile(path.join(projectPath, featureId, configPlugin)));
        // 3. Replace temporary id with id from plugin.xml
        featureId = featureConfigContent('id');
    }
    return featureId;
};

/**
 * @description Wrapper for {@link CordovaFeature.manage}
 * @param filePath {String} Path to Cordova project
 * @param featureType {String} Cordova features, such as plugin / platform / webview / browser
 * @param featureItems {String|Array} List of identifiers (e.g. 'org.apache.cordova.device')
 */
CordovaFeature.installFeature = function(filePath, featureType, featureItems) {
    CordovaFeature.manage(filePath, featureType, 'add', featureItems);
};

/**
 * @description Wrapper for {@link CordovaFeature.manage}
 * @param filePath {String} Path to Cordova project
 * @param featureType {String} Cordova features, such as plugin / platform / browser
 * @param featureItems {String|Array} List of identifiers (e.g. 'org.apache.cordova.device')
 */
CordovaFeature.removeFeature = function(filePath, featureType, featureItems) {
    CordovaFeature.manage(filePath, featureType, 'remove', featureItems);
};

/**
 * @description Execute Shell commands on a cordova-compatible binary
 * @param cliArguments {Array} CLI Arguments
 * @param cliName {String=} CLI Name
 * @requires module:cordova-lib
 */
CordovaFeature.execute = function(cliArguments, cliName) {

    /** @external */
    var path = require('path');

    /**  @defaults */
    var cliList = {
        'cordova': 'Apache Cordova',
        'phonegap': 'Adobe PhoneGap',
        'ionic': 'Ionic SDK',
        'cca': 'Chrome Apps for Mobile',
        'cocoonjs': 'CocoonJS'
    };

    // Select CLI
    var cliTitle = cliName || Object.keys(cliList)[0];

    // Get CLI Path
    var cli = nemo.fs.lookupExecutable(cliTitle);

    // Execute Command
    nemo.logger.log('Executing CLI Command', cliList[cliTitle]);
    nemo.util.runShell(cli, cliArguments);
};


/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = CordovaFeature;
