'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * JSON Schema (Draft 4) Handling
 * ========================================================================== */

// Init
var Schema = {};

/**
 * @description Create a JavaScript default object using a JSON Schema
 * @param filePath {String} Path to configuration template file (JSON schema)
 * @return {String|Boolean} nemo5 Configuration Template Object
 */
Schema.generateTemplate = function(filePath) {

    /** @external */
    var jsonschemadefaults = require('json-schema-defaults');

    return jsonschemadefaults(nemo.fs.getJSON(filePath));
};

/**
 * @description Pick property from a JSON Schema using a keypath.
 * @param schemaObject {Object} JSON Schema
 * @param propertyKeypath {String} Keypath to property
 * @param propertyName {String} Type of Schema property (id, type, default, description)
 * @return {String|Boolean} Value
 */
Schema.pickProperty = function(schemaObject, propertyKeypath, propertyName) {

    /** @external */
    var _ = require('lodash'),
        __ = require('underscore-keypath');

    /** @constant */
    var typesAllowed = ['id', 'type', 'default', 'description'];

    // Init
    var keypath = propertyKeypath,
        schemaReference = schemaObject,
        type = propertyName;

    if (!_.includes(typesAllowed, type)) {
        nemo.logger.error('(Internal Error) Invalid schema property.', null);
    }

    // TODO: Refactor
    keypath = keypath.replace(/.@first./g, '.items.');
    keypath = keypath.replace(/\./g, '.properties.');
    keypath = 'properties' + '.' + keypath + '.' + type;
    keypath = keypath.replace('.properties.items.properties', '.items.properties');

    return (__(schemaReference).valueForKeyPath(keypath));
};

/**
 * @description Get defaults from Schema
 * @param filePath {String} JSON Schema file
 * @param propertyKeypath {String} Keypath to property*
 * @return {String|Boolean} Value
 */
Schema.getDefault = function(filePath, propertyKeypath) {
    return Schema.pickProperty(filePath, propertyKeypath, 'default');
};

/**
 * @description Get description from Schema
 * @param filePath {String} JSON Schema file
 * @param propertyKeypath {String} Keypath to property
 * @return {String|Boolean} Value
 */
Schema.getDescription = function(filePath, propertyKeypath) {
    return Schema.pickProperty(filePath, propertyKeypath, 'description');
};

/**
 * @description Get type from Schema
 * @param filePath {String} JSON Schema file
 * @param propertyKeypath {String} Keypath to property
 * @return {String|Boolean} Value
 */
Schema.getListByExtension = function(filePath, propertyKeypath) {
    return Schema.pickProperty(filePath, propertyKeypath, 'type');
};

/**
 * @description Validate JSON files against Draft 4 Schema.
 * @param targetFilePath {String} Path to JSON file
 * @param schemaFilePath {String} Path to JSON Draft 4 Schema
 * @return {Object|Boolean} Validated object
 */
Schema.validateFile = function(targetFilePath, schemaFilePath) {

    /** @external */
    var tv4 = require('tv4');

    var targetContent = nemo.fs.getJSON(targetFilePath),
        schemaContent = nemo.fs.getJSON(schemaFilePath),
        result = tv4.validateMultiple(targetContent, schemaContent),
        resultList;

    if (!targetContent || !schemaContent) {
        return false;
    }

    if (result.valid === false) {

        // Remove stack traces from validation result
        resultList = result.errors.map(function(item) {
            return item['message'] + ' (at ' + item['dataPath'] + ')';
        });

        // Print Errors
        nemo.logger.errorList(resultList, 'JSON file did not pass internal validation. Please review.', targetFilePath);

        return false;
    }

    return targetContent;
};


/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = Schema;
