'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Platform Utilities, iOS
 * ========================================================================== */

// Init
var Platform = {};

/**
 * @description Add Platform
 */
Platform.ios = {};

/**
 * @description Packages iOS ".app" bundles as zipped ".ipa". Leverages the embedded Mobile Provisioning Profile for codesigning info.
 * @param filePath {String} Path to .app bundle
 * @param packagedFilePath {String=}
 * @returns {String|Boolean}
 */
Platform.ios.packageApplication = function(filePath, packagedFilePath) {

    /** @external */
    var path = require('path');

    /** @constants **/
    var packageCommand = nemo.fs.lookupExecutable('xcrun');

    /** @defaults */
    var defaultUnpackagedExtension = '.app',
        defaultPackagedExtension = '.ipa',
        defaultProfileFileName = 'embedded.mobileprovision',
        defaultProfileFile = path.join(filePath, defaultProfileFileName),
        defaultCertificateName = 'TeamName';

    // Init
    // Requires ."app" as directory, as they are bundles, not files
    var file = nemo.fs.normalize(filePath),
        filePackaged = packagedFilePath || path.join(path.dirname(file), path.basename(file).replace(defaultUnpackagedExtension, defaultPackagedExtension)),
        profileContent = nemo.fs.getProfile(defaultProfileFile),
        certificateName = profileContent[defaultCertificateName],
        packageArgs = ['-sdk', 'iphoneos', 'PackageApplication', '-v', file, '-o', filePackaged, '--embed', defaultProfileFile, '--sign', certificateName],
        packageResult;

    // Package & Resign
    packageResult = nemo.util.getShell(packageCommand, packageArgs);

    if (!packageResult) {
        return false;
    }

    return filePackaged;
};

/**
 * @description Convert list of Cordova devicetypes to XCode value
 * @param devicetypes {Array} Options: handset, tablet
 * @returns {Object|Boolean}
 */
Platform.ios.getDevicetype = function(devicetypes) {

    /** @external */
    var _ = require('lodash');

    /** @constants */
    var devicetypeMap = {
        'handset': '1',
        'tablet': '2'
    };

    // Init
    var devicetypeList = devicetypes,
        devicetypeResolved;

    // Fallback
    devicetypes.forEach(function(item) {
        if (!_.contains(Object.keys(devicetypeMap), item)) {
            nemo.logger.error('Cordova device type not recognized: ' + '"' + item + '"', devicetypeList.join());
            devicetypeList = Object.keys(devicetypeMap);
        }
    });

    // Default / Fallback
    if (devicetypeList.length !== 1) {
        devicetypeResolved = devicetypeMap[devicetypeList[0]] + ',' + devicetypeMap[devicetypeList[1]];
    }

    // Convert
    if (devicetypeList.length === 1) {
        devicetypeResolved = devicetypeMap[devicetypeList[0]];
    }

    return devicetypeResolved;
};

/**
 * @description Install iOS mobile provisioning profile
 * @param filePath {String} .p12 Certificate
 * @returns {Object|Boolean}
 */
Platform.ios.installProfile = function(filePath) {
    return nemo.system.installProfile(filePath);
};

/**
 * @description Add private key to existing nemo.osX keychains for codesigning iOS & nemo.osX apps.
 * @param filePath {String} .p12 Certificate
 * @param filePassword {String} Certificate password
 * @param temporaryKeychain {Boolean=} Use the default system Keychain instead of creating a temporary one
 * @returns {Object|Boolean}
 */
Platform.ios.installCertificate = function(filePath, filePassword, temporaryKeychain) {

    if (temporaryKeychain === true) {
        return nemo.system.installTemporaryCertificate(filePath, filePassword);
    }

    return nemo.system.installCertificate(filePath, filePassword);
};


/**
 * @description Adds a supported device orientation to iOS project
 * @param filePath {String} Path to iOS Project
 * @param targetDevicetype {'handset', 'tablet'} Devicetype
 * @param targetOrientation {'portrait', 'landscape'} Orientation
 * @param projectName {String} Project Name
 * @returns {Object|Boolean}
 */
Platform.ios.addOrientation = function(filePath, targetDevicetype, targetOrientation, projectName) {

    /** @external */
    var path = require('path'),
        _ = require('lodash');

    /** @constants */
    var defaultProjectConfigurationFileNameSuffix = '-Info.plist';

    // Resolve iOS Orientation Enum
    var platformOrientationOptions = {
            'handset': {
                'landscape': {
                    'UISupportedInterfaceOrientations~iphone': ['UIInterfaceOrientationLandscapeLeft', 'UIInterfaceOrientationLandscapeRight']
                },
                'portrait': {
                    'UISupportedInterfaceOrientations~iphone': ['UIInterfaceOrientationPortrait', 'UIInterfaceOrientationPortraitUpsideDown']
                }
            },
            'tablet': {
                'landscape': {
                    'UISupportedInterfaceOrientations~ipad': ['UIInterfaceOrientationPortrait', 'UIInterfaceOrientationPortraitUpsideDown']
                },
                'portrait': {
                    'UISupportedInterfaceOrientations~ipad': ['UIInterfaceOrientationLandscapeLeft', 'UIInterfaceOrientationLandscapeRight']
                }
            }
        };

    // Convert Platform Configuration Object from Platform Configuration File
    var projectPath = filePath,
        projectConfigurationFileName = projectName + defaultProjectConfigurationFileNameSuffix,
        projectConfigurationFile = path.join(projectPath, projectName, projectConfigurationFileName),
        projectConfigurationContent = nemo.fs.getPlist(projectConfigurationFile);

    // Init Orientation
    var platformOrientationConfiguration,
        orientationResult;

    // Check if provided Orientation is within known range
    if (!_.includes(Object.keys(platformOrientationOptions[targetDevicetype]), targetOrientation)) {
        nemo.logger.error('(Internal Error) Orientation name unknown.', targetOrientation);
        return false;
    }

    // Generate platform-specific orientation-configuration sub-object
    platformOrientationConfiguration = platformOrientationOptions[targetDevicetype][targetOrientation];

    // Add orientation-configuration sub-object to Platform Configuration Object
    projectConfigurationContent = _.merge(projectConfigurationContent, platformOrientationConfiguration);

    // Write Platform Configuration Object to Platform Configuration File
    orientationResult = nemo.fs.writePlist(projectConfigurationFile, projectConfigurationContent);

    // Check
    return orientationResult;
};

 /**
 * @description Resets orientation settings in iOS projects
 * @param filePath {String} Path to iOS Project
 * @param projectName {String} Project Name
 * @returns {Object|Boolean}
 */
Platform.ios.resetOrientations = function(filePath, projectName) {

    /** @external */
    var path = require('path'),
        _ = require('lodash');

    /** @constants */
    var defaultProjectConfigurationFileNameSuffix = '-Info.plist';

    // Platform-specific orientation property names
    var platformOrientationProperties = ['UIInterfaceOrientation', 'UISupportedInterfaceOrientations', 'UISupportedInterfaceOrientations~iphone', 'UISupportedInterfaceOrientations~ipad'];

    // Convert Platform Configuration Object from Platform Configuration File
    var projectPath = filePath,
        projectConfigurationFileName = projectName + defaultProjectConfigurationFileNameSuffix,
        projectConfigurationFile = path.join(projectPath, projectName, projectConfigurationFileName),
        projectConfigurationContent = nemo.fs.getPlist(projectConfigurationFile);

    // Init Orientation
    var orientationResult;

    // Remove all platform-specific orientation properties
    _.each(platformOrientationProperties, function(value) {
        if (projectConfigurationContent.hasOwnProperty(value)) {
            delete projectConfigurationContent[value];
        }
    });

    // Write Platform Configuration Object to Platform Configuration File
    orientationResult = nemo.fs.writePlist(projectConfigurationFile, projectConfigurationContent);

    // Check
    return orientationResult;
};




/* ==========================================================================
 * Platform Utilities, Android
 * ========================================================================== */

/**
 * @description Add Platform
 */
Platform.android = {};


/**
 * @description Packages and aligns ".apk" files.
 * @param filePath {String} Path to .apk file
 * @param packagedFilePath {String=}
 * @returns {String|Boolean}
 */
Platform.android.packageApplication = function(filePath, packagedFilePath) {

    /** @external */
    var path = require('path'),
        fs = require('fs');

    /** @constants **/
    var packageCommand = path.join(nemo.system.getAndroidBuildtools(), 'zipalign');

    /** @defaults */
    var defaultTemporarySuffix = '.unpackaged',
        defaultAlignment = 4;

    // Init
    var file = nemo.fs.normalize(filePath),
        fileTemporary = file + defaultTemporarySuffix,
        filePackaged = packagedFilePath || file,
        packageArgs = ['-f', '-v', defaultAlignment, fileTemporary, filePackaged],
        packageResult;

    // Rename
    fs.renameSync(file, fileTemporary);

    // Package & Resign
    packageResult = nemo.util.getShell(packageCommand, packageArgs);

    if (!packageResult) {
        return false;
    }

    return filePackaged;
};

/**
 * @description Set allowed orientations within Android configuration
 * @param filePath {String} Path to Android Manifest
 * @param orientation {String=} Target Orientation
 * @returns {Boolean} Result
 */
Platform.android.addOrientation = function(filePath, orientation) {

    /** @external */
    var _ = require('lodash');

    /** @constants **/
    var orientationAll = nemo.cordovainfo.resolveOrientation('ORIENTATION_GLOBAL_ORIENTATIONS');

    // Init
    var fileData = nemo.fs.readFile(filePath),
        fileContent = nemo.util.parseXml(fileData),
        targetOrientation = orientation,
        orientationResult;

    // Apply target Configuration via platform enum
    if (_.contains(orientationAll, orientation)) {
        fileContent['app']['activity']['android:screenOrientation'] = targetOrientation;
    } else {
        delete fileContent['app']['activity']['android:screenOrientation'];
    }

    // Write Manifest
    orientationResult = nemo.fs.writeXML(filePath, fileContent);

    return orientationResult;
};


/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = Platform;
