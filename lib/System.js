'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Operating System
 * ========================================================================== */

// Init
var System = {};

/**
 * @description Add keys to default OSX Keychain
 * @param filePath {String}
 * @param filePassword {String}
 * @returns {String|Boolean}
 */
System.installCertificate = function(filePath, filePassword) {

    /** @external */
    var path = require('path');

    /** @constants **/
    var certificateCommand = nemo.fs.lookupExecutable('security');

    /** @defaults **/
    var defaultKeychain = 'Library/Keychains/login.keychain',
        defaultKeychainFile = path.join(process.env.HOME, defaultKeychain);

    // Init
    var file = nemo.fs.isFile(filePath),
        password = filePassword,
        keychainName = defaultKeychainFile,
        args = ['import', file, '-k', keychainName, '-f', 'pkcs12', '-P', password, '-A'],
        installResult;

    // Import certificate
    installResult = nemo.util.getShell(certificateCommand, args);

    // Feedback
    if (!installResult) {
        return false;
    }

    return installResult;
};


/**
 * @description Create temporary OSX Keychain and add certificates to them
 * @param filePath {String}
 * @param certificatePassword {String}
 * @returns {String|Boolean}
 */
System.installTemporaryCertificate = function(filePath, certificatePassword) {

    /** @external */
    var path = require('path');

    /** @defaults **/
    var currentKeychainFile,
        temporaryKeychainFileNamePrefix = 'nemo5',
        temporaryKeychainFileExtension = '.keychain',
        temporaryKeychainPassword = temporaryKeychainFileNamePrefix;

    /** @constants **/
    var certificateCommand = nemo.fs.lookupExecutable('security');

    /** @defaults **/
    var keychainFileName = temporaryKeychainFileNamePrefix + temporaryKeychainFileExtension,
        keychainFile = path.join(process.env.HOME, 'Library', 'Keychains', keychainFileName);

    // Init
    var certificateFile = nemo.fs.isFile(filePath),
        installResult;

    // 1. Save current default Keychain
    currentKeychainFile = nemo.util.getShell(certificateCommand, ['default-keychain']);

    // 2. Delete temporary Keychain
    nemo.util.getShell(certificateCommand, ['delete-keychain', keychainFile]);

    // 3. Create temporary Keychain
    nemo.util.getShell(certificateCommand, ['create-keychain', '-p', temporaryKeychainPassword, keychainFile]);

    // 4. Set default Keychain to temporary Keychain
    nemo.util.getShell(certificateCommand, ['default-keychain', '-s', keychainFile]);

    // 5. Unlock temporary Keychain
    nemo.util.getShell(certificateCommand, ['unlock-keychain', '-p', temporaryKeychainPassword, keychainFile]);

    // 6. Import certificate to temporary Keychain
    installResult = nemo.util.getShell(certificateCommand, ['import', certificateFile, '-k', keychainFile, '-f', 'pkcs12', '-P', certificatePassword, '-A']);

    // 7. Reset default Keychain setting
    nemo.util.getShell(certificateCommand, ['default-keychain', '-s', currentKeychainFile]);

    // Feedback
    if (!installResult) {
        nemo.logger.error('Could not import iOS certificate to temporary Keychain: ' + path.basename(keychainFile), certificateFile);
        return false;
    }
    nemo.logger.log('Successfully imported iOS certificate to temporary Keychain: ' + path.basename(keychainFile), certificateFile);
    return installResult;
};


/**
 * @description Test if a codesigning certificate (private/public key) is available in the active keychain
 * @param filePath {String}
 * @param filePassword {String}
 * @returns {String|Boolean} Name of certificate holder
 */
System.testCertificate = function(filePath, filePassword) {

    /** @external */
    var _ = require('lodash'),
        path = require('path');

    /** @constants **/
    var searchCommand = nemo.fs.lookupExecutable('security');

    /** @defaults **/
    var defaultKeychain = 'Library/Keychains/login.keychain',
        defaultKeychainFile = path.join(process.env.HOME, defaultKeychain);

    // Build a list of the sha-1 hashes of all valid codesigning identities (certificates) currently installed.
    // Test if hash is contained in installed certificates.
    var certificateData = nemo.fs.readSSL(filePath, filePassword),
        certificateContent = nemo.fs.getCertificate(filePath, filePassword),
        certificateName = certificateContent['subject']['commonName'],
        keychainSearchArgs = ['find-identity', '-v', '-p', 'codesigning', defaultKeychainFile],
        keychainSearchResult = nemo.util.getShell(searchCommand, keychainSearchArgs, 'utf8'),
        certificateHash = nemo.util.hashCertificate(certificateData),
        certificateList = keychainSearchResult.match(/([a-f0-9]{40})/igm),
        testResult = _.contains(certificateList, certificateHash);

    // Test if hash is contained in list of installed certificates
    if (!testResult) {
        return false;
    }

    // Return name of certificate if found
    return certificateName;
};

/**
 * @description Install iOS Mobile Provisioning Profile
 * @param filePath {String} .mobileprovision
 * @returns {String|Boolean}
 */
System.installProfile = function(filePath) {

    /** @external */
    var path = require('path'),
        fse = require('fs-extra');

    /** @defaults **/
    var defaultExtension = '.mobileprovision',
        defaultFolder = path.join(process.env['HOME'], 'Library/MobileDevice/Provisioning Profiles');

    // Init
    var file = nemo.fs.isFile(filePath),
        profileContent = nemo.fs.getProfile(file),
        profileId = profileContent['UUID'],
        name = profileId + defaultExtension,
        targetFile = path.join(defaultFolder, name),
        installResult;

    // Test if Mobile Provisioning Profile was imported
    fse.copySync(file, targetFile);
    installResult = nemo.fs.isFile(targetFile);

    // Feedback
    if (!installResult) {
        return false;
    }

    return installResult;
};


/**
 * @description Test if a iOS Mobile Provisioning Profile has been installed
 * @param filePath {String}
 * @returns {String|Boolean}
 */
System.testProfile = function(filePath) {

    /** @external */
    var path = require('path');

    /** @defaults **/
    var defaultProfileFolder = path.join(process.env['HOME'], 'Library/MobileDevice/Provisioning Profiles');

    // Init
    var file = nemo.fs.isFile(filePath),
        profileContent = nemo.fs.getProfile(file),
        profileId = profileContent['UUID'],
        profileName = profileContent['Name'],
        name = profileId + path.extname(file),
        targetFile = path.join(defaultProfileFolder, name),
        testResult = nemo.fs.isExisting(targetFile);

    // Test if hash is contained in list of installed certificates
    if (!testResult) {
        return false;
    }

    // Return name of certificate if found
    return profileName;
};

/**
 * @description Retrieves Path of current Android Build Tools
 * @returns {String|Boolean}
 */
System.getAndroidBuildtools = function() {

    /** @external */
    var path = require('path'),
        semver = require('semver');

    /** @default **/
    var defaultBuildtoolsFolderName = 'build-tools';

    // Init
    var sdkFolder = System.getAndroidSdk();

    if (!System.getAndroidSdk()) {
        return false;
    }

    // Retrieve latest local build tools folder, using the location of the "android" command as base for lookup.
    var buildtoolsFolder = path.join(sdkFolder, defaultBuildtoolsFolderName),
        buildtoolsList = nemo.fs.getFolders(buildtoolsFolder),
        buildtoolsListSorted = buildtoolsList
            .filter(function(i) {
                return semver.valid(i);
            }).sort(function(a, b) {
                return semver.rcompare(a, b);
            }),
        buildtoolsCurrent = buildtoolsListSorted[0],
        buildtoolsResult;

    // Return path of most current version
    buildtoolsResult = path.join(buildtoolsFolder, buildtoolsCurrent);

    return buildtoolsResult;
};

/**
 * @description Try to retrieve location of Android SDK
 * @returns {String|Boolean}
 */
System.getAndroidSdk = function() {

    /** @external */
    var path = require('path'),
        _ = require('lodash');

    /** @constants */
    var expectedFolderNameList = [
        path.join('android'),
        path.join('android-sdk'),
        path.join('android-sdk-macosx'),
        path.join('Android', 'android'),
        path.join('Android', 'android-sdk'),
        path.join('Android', 'android-studio', 'sdk'),
        path.join('Android', 'sdk'),
        path.join('Android Studio.app', 'sdk'),
        path.join('Android Studio.app', 'Contents', 'sdk')
    ];

    // Init
    var expectedFolderList = _.union(nemo.fs.prependPath(expectedFolderNameList, System.getApplicationsDirectory()), nemo.fs.prependPath(expectedFolderNameList, System.getSettingsDirectory())),
        sdkResult;

    // 1. Try the  "ANDROID_HOME" environment variable
    if (System.getAndroidHome()) {
        sdkResult = System.isAndroidSdk(System.getAndroidHome());
    }

    // 2. Try looking up directories and folders in Applications / Settings folders
    if (!System.getAndroidHome()) {
        _.each(expectedFolderList, function(folder) {
            if (nemo.fs.isExisting(folder) && System.isAndroidSdk(folder)) {
                process.env['ANDROID_HOME'] = folder;
                sdkResult = folder;
            }
        });
    }

    // Error
    if (!sdkResult) {
        nemo.logger.error('(Internal Error) Could not find Android SDK folder.', process.platform);
        return false;
    }

    return sdkResult;
};

/**
 * @description Check whether a folder is likely to contain Android SDK
 * @param filePath {String}
 * @returns {String|Boolean}
 */
System.isAndroidSdk = function(filePath) {

    /** @external */
    var path = require('path'),
        _ = require('lodash');

    /** @constants */
    var expectedExecutableNameListWindows = [
            path.join('platform-tools', 'adb.exe'),
            path.join('tools', 'android.exe')
        ],
        expectedExecutableNameList = [
            path.join('platform-tools', 'adb'),
            path.join('tools', 'android')
        ];

    // Init
    var folder = nemo.fs.normalize(filePath),
        expectedFiles = System.isWindows() === true ? expectedExecutableNameListWindows : expectedExecutableNameList,
        sdkSearch = _.every(expectedFiles, function(file) {
            return nemo.fs.isExisting(path.join(folder, file));
        });

    if (!sdkSearch) {
        return false;
    }

    return folder;
};


/**
 * @description Checks if we are running on Microsoft Windows
 * @returns {Boolean}
 */
System.isWindows = function() {

    /** @external */
    var _ = require('lodash');

    /** @constants */
    var osNameTarget = 'win';

    // Init
    var osNameCurrent = process.platform;

    return _.startsWith(osNameCurrent, osNameTarget);
};


/**
 * @description Checks if we are running on Apple OSX
 * @returns {Boolean}
 */
System.isOsx = function() {

    /** @external */
    var _ = require('lodash');

    /** @constants */
    var osNameTarget = 'darwin';

    // Init
    var osName = process.platform;

    return _.startsWith(osName, osNameTarget);
};


/**
 * @description Checks if we are running on Linux
 * @returns {Boolean}
 */
System.isLinux = function() {

    /** @external */
    var _ = require('lodash');

    /** @constants */
    var osNameTarget = 'linux';

    // Init
    var osName = process.platform;

    return _.startsWith(osName, osNameTarget);
};


/**
 * @description Checks if 'ANDROID_HOME' 1) is set and b) represents a valid filesystem location
 * @returns {String|Boolean}
 */
System.getAndroidHome = function() {

    /** @constants */
    var environmentConstant = 'ANDROID_HOME';

    // Init
    var environmentResult = process.env[environmentConstant],
        pathResult = nemo.fs.isDirectory(environmentResult);

    if (!pathResult) {
        return false;
    }

    return pathResult;
};

/**
 * @description Gets OS-specific Applications folder
 * @returns {String|Boolean}
 */
System.getApplicationsDirectory = function() {

    /** @defaults */
    var defaultApplicationsDirectoryWindows = process.env['ProgramFiles'],
        defaultApplicationsDirectoryOsx = '/Applications',
        defaultApplicationsDirectoryLinux = '/usr/bin';

    // Windows
    if (System.isWindows()) {
        return defaultApplicationsDirectoryWindows;
    }

    // OSX
    if (System.isOsx()) {
        return defaultApplicationsDirectoryOsx;
    }

    // Linux
    if (System.isLinux()) {
        return defaultApplicationsDirectoryLinux;
    }

    // Error
    nemo.logger.error('(Internal Error) Could not find system Applications folder.', process.platform);

    return false;
};


/**
 * @description Gets OS-specific User Settings folder
 * @returns {String|Boolean}
 */
System.getSettingsDirectory = function() {

    /** @external */
    var path = require('path');

    /** @defaults */
    var defaultSettingsDirectoryWindows = process.env['LOCALAPPDATA'],
        defaultSettingsDirectoryOsx = path.join(process.env['HOME'], 'Library'),
        defaultSettingsDirectoryLinux = '/etc';

    // Windows
    if (System.isWindows()) {
        return defaultSettingsDirectoryWindows;
    }

    // OSX
    if (System.isOsx()) {
        return defaultSettingsDirectoryOsx;
    }

    // Linux
    if (System.isLinux()) {
        return defaultSettingsDirectoryLinux;
    }

    // Error
    nemo.logger.error('(Internal Error) Could not find system Settings folder.', process.platform);

    return false;
};

/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = System;
