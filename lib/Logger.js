'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Logging nemo.utilities
 * ========================================================================== */

// Init
var Logger = {};

/**
 * @description nemo.terminal logger with text fx
 * @param messageRaw {String}
 * @param messageVariableRaw {String=}
 * @param messageTitleRaw {String=}
 * @param messageErrorRaw {Boolean=}
 */
Logger.write = function(messageRaw, messageVariableRaw, messageTitleRaw, messageErrorRaw) {

    /** @external */
    var br = require('os').EOL,
        chalk = require('chalk');

    // Init
    var logPrefix = '',
        logError = messageErrorRaw,
        logTitle = messageTitleRaw,
        logVariable = messageVariableRaw,
        logMessage = messageRaw;


    if (nemo.util.isEmpty(messageRaw)) {
        return;
    }

    if (!logError) {
        if (!nemo.util.isEmpty(logTitle)) {
            logTitle = nemo.terminal.wrapColumns(chalk.dim.cyan(logTitle), chalk.dim.cyan(logPrefix));
        }
        if (nemo.util.isEmpty(logVariable)) {
            logMessage = nemo.terminal.wrapLine(chalk.bold.cyan(logMessage));
        } else {
            logMessage = nemo.terminal.wrapLine(chalk.bold.cyan(logMessage) + chalk.dim.cyan(' (' + logVariable + ')'));
        }
    } else {
        if (!nemo.util.isEmpty(logTitle)) {
            logTitle = nemo.terminal.wrapColumns(chalk.dim.red(logTitle), chalk.dim.red(logPrefix));
        }
        if (nemo.util.isEmpty(logVariable)) {
            logMessage = nemo.terminal.wrapLine(chalk.bold.red(logMessage));
        } else {
            logMessage = nemo.terminal.wrapLine(chalk.bold.red(logMessage) + chalk.dim.red(' (' + logVariable + ')'));
        }
    }

    /** Print */

    if (!nemo.util.isEmpty(logTitle)) {
        process.stdout.write(logTitle);
    }
    process.stdout.write(logMessage);
    process.stdout.write(br);
    process.stdout.write(br);

    return true;

};

/**
 * @description Prints full-width title
 * @param heading {String} Message
 * @param prefix {String=} Title prefix
 */
Logger.heading = function(heading, prefix) {

    /** @external */
    var br = require('os').EOL,
        chalk = require('chalk');

    var borderTop = '▀▀▀▀▀',
        borderBottom = '▄▄▄▄▄',
        logMessage;

    if (nemo.util.isEmpty(prefix)) {
        logMessage = nemo.terminal.wrapLine(chalk.bold.cyan(heading));
    } else {
        logMessage = nemo.terminal.wrapLine(chalk.dim.cyan(prefix.toUpperCase())) + br + nemo.terminal.wrapLine(chalk.bold.cyan(heading));
    }

    // Clear nemo.terminal
    //process.stdout.write('\u001B[2J\u001B[0;0f');

    process.stdout.write(br);
    process.stdout.write(chalk.styles.dim.open + chalk.styles.cyan.open + nemo.terminal.wrapLine(borderTop, borderTop[0]) + chalk.styles.cyan.close + chalk.styles.dim.close);
    process.stdout.write(br);
    process.stdout.write(logMessage);
    process.stdout.write(br);
    process.stdout.write(chalk.styles.dim.open + chalk.styles.cyan.open + nemo.terminal.wrapLine(borderBottom, borderBottom[0]) + chalk.styles.cyan.close + chalk.styles.dim.close);
    process.stdout.write(br);
    process.stdout.write(br);
};

/**
 * @description Logs output
 * @param msg {String}
 * @param msgVar {String}
 * @param msgTitle {String=}
 */
Logger.log = function(msg, msgVar, msgTitle) {
    Logger.write(msg, msgVar, msgTitle);
};

/**
 * @description Logs output without annotations
 * @param msg {*}
 * @param msgVar {String=}
 * @param msgTitle {String=}
 */
Logger.logData = function(msg, msgVar, msgTitle) {
    Logger.write(msg, msgVar, msgTitle);
};

/**
 * @description Logs errors to the terminal
 * @param msg {String}
 * @param msgVar {String}
 * @param msgTitle {String=}
 */
Logger.error = function(msg, msgVar, msgTitle) {
    Logger.write(msg, msgVar, msgTitle, Boolean(true));
};

/**
 * @description Logs list of errors to the terminal
 * @param errorList {Array}
 * @param msgLabel {String}
 * @param msgVar {String}
 * @param msgTitle {String=}
 */
Logger.errorList = function(errorList, msgLabel, msgVar, msgTitle) {

    /** @external */
    var br = require('os').EOL;

    // Init
    var numberedList = errorList.map(function(item, index) {
            return '     ' + (index + 1) + ') ' + item;
        }),
        list = numberedList.join(br),
        msg = msgLabel + br + list + br + '  in: ' + msgVar + br;

    Logger.write(msg, null, msgTitle, Boolean(true));
};

/**
 * @description Logs error messages without annotations
 * @param msg {String, Object}
 * @param msgVar {String=}
 * @param msgTitle {String=}
 */
Logger.errorData = function(msg, msgVar, msgTitle) {
    Logger.write(msg, msgVar, msgTitle, Boolean(true));
};


/**
 * @description Logs errors to the terminal. Aborts afterwards.
 * @param msg {String=}
 * @param msgVar {String=}
 * @param msgTitle {String=}
 */
Logger.fail = function(msg, msgVar, msgTitle) {

    /** @external */
    var br = require('os').EOL,
        chalk = require('chalk'),
        path = require('path');

    var thisProcessName = path.basename(process.env['_']);

    if (msg) {
        Logger.error(msg, msgVar, msgTitle);
    }

    process.stdout.write(nemo.terminal.wrapLine(chalk.inverse.bold.red('Aborting ' + thisProcessName + '.')));
    process.stdout.write(br);
    process.stdout.write(br);

    process.exit(1);
};


/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = Logger;
