'use strict';

var nemo = require('./index.js');

/* ==========================================================================
 * Cordova Info
 * ========================================================================== */

// Init
var CordovaInfo = {};

/**
 * @description Check whether a valid Cordova project can be found at the given path.
 * @param filePath {String} Path to check
 * @returns {Boolean|String}
 */
CordovaInfo.getCordova = function(filePath) {

    /** @external */
    var cordovaUtility = require('cordova-lib/src/cordova/util');

    // Init
    var targetPath = nemo.fs.normalizePath(filePath),
        cordovaResult;

    // Validate Cordova project
    cordovaResult = cordovaUtility.isCordova(targetPath);

    if (!cordovaResult) {
        return false;
    }

    return cordovaResult;
};

/**
 * @description Read application version at provided Cordova project path.
 * @param filePath {String} Path to Cordova project
 * @returns {String}
 */
CordovaInfo.getAppVersion = function(filePath) {

    // Init
    var settingName = 'version';

    return CordovaInfo.getConfiguration(filePath, settingName);
};

/**
 * @description Read application name at provided Cordova project path.
 * @param filePath {String} Path to Cordova project
 * @returns {String}
 */
CordovaInfo.getAppName = function(filePath) {

    // Init
    var settingName = 'name';

    return CordovaInfo.getConfiguration(filePath, settingName);
};

/**
 * @description Read the reverse domain-style application identifier at provided Cordova project path.
 * @param filePath {String} Path to Cordova project
 * @returns {String}
 */
CordovaInfo.getAppId = function(filePath) {

    // Init
    var settingName = 'packageName';

    return CordovaInfo.getConfiguration(filePath, settingName);
};

/**
 * @description Retrieve a configuration attribute of Cordova project at provided path.
 * @param filePath {String} Path to Cordova Project
 * @param name {String} Name of Cordova setting
 * @returns {*}
 */
CordovaInfo.getConfiguration = function(filePath, name) {

    /** @external */
    var ConfigUtility = require('cordova-lib/src/cordova/util'),
        ConfigParser = require('cordova-lib/src/configparser/ConfigParser.js');

    // Init
    var projectPath = nemo.fs.normalize(filePath),
        configurationFile = ConfigUtility.projectConfig(projectPath),
        configurationReader = new ConfigParser(configurationFile),
        attributeName = name,
        settingResult;

    // Read project configuration value
    settingResult = configurationReader[attributeName]();

    return settingResult;
};

/**
 * @description Write a global configuration value to Cordova project at provided path.
 * @param filePath {String} Path to Cordova Project
 * @param name {String} Name of Cordova setting
 * @param value {String} Value of Cordova setting
 * @returns {Array}
 */
CordovaInfo.setConfiguration = function(filePath, name, value) {

    /** @external */
    var ConfigUtility = require('cordova-lib/src/cordova/util'),
        ConfigParser = require('cordova-lib/src/configparser/ConfigParser.js');

    // Init
    var projectPath = nemo.fs.normalize(filePath),
        configurationFile = ConfigUtility.projectConfig(projectPath),
        configurationWriter = new ConfigParser(configurationFile),
        attributeName = name,
        attributeValue = value.trim(),
        settingResult;

    // Set project configuration value
    settingResult = configurationWriter.setGlobalPreference(attributeName, attributeValue);

    return settingResult;
};

/**
 * @description Retrieve id of plugins installed at provided Cordova project path (returns 'false' if no plugins are installed).
 * @param filePath {String} Path to Cordova Project
 * @returns {Array|Boolean}
 */
CordovaInfo.getPluginsInstalled = function(filePath) {

    /** @external */
    var path = require('path'),
        cordovaUtility = require('cordova-lib/src/cordova/util');

    /** @default */
    var pluginPathName = 'plugins';

    // Init
    var projectPath = nemo.fs.normalize(filePath),
        pluginsPath = path.join(projectPath, pluginPathName),
        pluginResult;

    // List plugins
    pluginResult = cordovaUtility.findPlugins(pluginsPath);

    if (nemo.util.isEmpty(pluginResult)) {
        return false;
    }

    return pluginResult;
};

/**
 * @description Retrieve id, version, variables of plugins saved in configuration file of Cordova project at path (returns 'false' if none).
 * @param filePath {String} Path to Cordova Project
 * @returns {Promise.<{id: string, version: string, variables: {name: string, value: string}[]}[]>|*}
 */
CordovaInfo.getPluginsSaved = function(filePath) {

    /** @external */
    var cordovaProjectMetadata = require('cordova-lib/src/cordova/project_metadata');

    // Init
    var projectPath = nemo.fs.normalize(filePath),
        pluginResult;

    // List plugins
    pluginResult = cordovaProjectMetadata.getPlugins(projectPath)
        .then(function(value) {
            return value;
        });

    if (nemo.util.isEmpty(pluginResult)) {
        return false;
    }

    return pluginResult;
};

/**
 * @description Retrieve names of all installed Cordova platforms at provided path. Returns false if no platforms are installed.
 * @param filePath {String} Path to Cordova Project
 * @returns {Array|Boolean} List of installed platform names
 */
CordovaInfo.getPlatformsInstalled = function(filePath) {

    /** @external */
    var cordovaUtility = require('cordova-lib/src/cordova/util');

    // Init
    var projectPath = nemo.fs.normalize(filePath),
        platformResult;

    // List installed platforms
    platformResult = cordovaUtility.listPlatforms(projectPath);

    if (nemo.util.isEmpty(platformResult)) {
        return false;
    }

    return platformResult;
};


/**
 * @description Retrieve name, version of platforms saved in configuration file of Cordova project at path (returns 'false' if none).
 * @param filePath {String} Path to Cordova Project
 * @returns {Promise.<{name: string, version: string, src: string}[]>|*}
 */
CordovaInfo.getPlatformsSaved = function(filePath) {

    /** @external */
    var cordovaProjectMetadata = require('cordova-lib/src/cordova/project_metadata');

    // Init
    var projectPath = nemo.fs.normalize(filePath),
        platformResult;

    // List platforms
    platformResult = cordovaProjectMetadata.getPlatforms(projectPath)
        .then(function(value) {
            return value;
        });

    if (nemo.util.isEmpty(platformResult)) {
        return false;
    }

    return platformResult;
};

/**
 * @description Retrieve names of all available Cordova platforms.
 * @returns {Array} List of installed platform names
 */
CordovaInfo.getPlatformsAvailable = function() {

    /** @external */
    var cordovaPlatforms = require('cordova-lib/src/platforms/platforms');

    // Init
    var platformResult;

    // List platforms
    platformResult = Object.keys(cordovaPlatforms);

    return platformResult;
};

/**
 * @description Completely removes all plugins or platforms from filesystem
 * @param filePath {String} Path to Cordova project
 * @param component {plugin, platform} Component name
 * @returns {Boolean}
 */
CordovaInfo.deleteComponent = function(filePath, component) {

    /** @external */
    var path = require('path');

    // Init
    var componentName = component,
        projectPath = nemo.fs.normalizePath(filePath),
        componentFolder = path.join(projectPath, componentName),
        deleteResult;

    // Remove all contents of component folder
    deleteResult = nemo.fs.deletePathContent(componentFolder);

    return deleteResult;
};

/**
 * @description Resolve Cordova Orientation enums
 * @param orientationEnum {'ORIENTATION_DEFAULT','ORIENTATION_PORTRAIT','ORIENTATION_LANDSCAPE','ORIENTATION_GLOBAL_ORIENTATIONS'}
 * @returns {'default'|'portrait'|'landscape'|['default', 'portrait', 'landscape']}
 */
CordovaInfo.resolveOrientation = function(orientationEnum) {

    /** @external */
    var CordovaOrientation = require('cordova-lib/src/cordova/metadata/parserhelper/preferences');

    // Init
    var orientation = orientationEnum,
        orientationName;

    // Resolve
    orientationName = CordovaOrientation[orientation];

    return orientationName;
};


/* ==========================================================================
 * Module Export
 * ========================================================================== */

module.exports = CordovaInfo;
