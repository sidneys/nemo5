'use strict';

/**
 * @summary NodeJS Utilities for Neofonie Mobile projects
 * @author S. Bofah
 * @created 2015-02-01
 */

var nemo = exports;

nemo.terminal = require('./Terminal');
nemo.logger = require('./Logger');
nemo.util = require('./Util');
nemo.schema = require('./Schema');
nemo.system = require('./System');
nemo.cordovainfo = require('./CordovaInfo');
nemo.cordovafeature = require('./CordovaFeature');
nemo.fs = require('./Filesystem');
nemo.platform = require('./Platform');
nemo.git = require('./Git');


nemo.log = require('./Logger').log
nemo.error = require('./Logger').error
nemo.fail = require('./Logger').fail
