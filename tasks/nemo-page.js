'use strict';

/**
 * @summary Generates a HTML release download page from template files and JSON-based configuration.
 * @author Sidney Bofah
 * @created 2015-02-01
 */
module.exports = function(grunt) {

    grunt.registerTask('nemo-page', 'Page', function() {

        /** @external */
        var path = require('path'),
            nemo = require('../lib/index.js'),
            _ = require('lodash'),
            urljoin = require('url-join'),
            replaceExt = require('replace-ext');

        /** @constant */
        var _TITLE = 'Component: Page';

        /** @default */
        var _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];


        // Environment
        var options = grunt.config.get('nemo')['options']['page'],
            outputDirectory = _PLATFORMDATA['output_dir'];

        // Page Template
        var pageTitle = nemo.cordovainfo.getAppName(grunt.ENV.APP_FILEPATH),
            pageIndexTemplate = grunt.RESOURCE.HOME(_PLATFORMDATA['page_index_file']),
            pageAssetsTemplate = grunt.RESOURCE.HOME(_PLATFORMDATA['page_assets_dir']);

        // Status
        nemo.logger.heading(_TITLE, 'Starting Task');


        /**
         * BUILD LIST OF APPLICATION DOWNLOADS
         */
        // List Executables (from binaries in output path)
        var defaultBinaryExtensionList = ['apk', 'ipa'],
            binaryQueue = nemo.fs.getFilesDeep(outputDirectory, defaultBinaryExtensionList);

        // List Platforms (from folders in output path)
        var defaultPlatformNameList = nemo.cordovainfo.getPlatformsAvailable(),
            platformList = nemo.fs.getFoldersDeep(outputDirectory, defaultPlatformNameList);

        // Create List of Application Downloads
        var applicationList = _.map(binaryQueue, function(filePath) {
            // Get Platform
            var platformName = path.basename(path.dirname(filePath));
            // Get Icon
            var iconFile = replaceExt(filePath, '.png');

            var platform = {
                file: filePath,
                version: grunt.ENV.APP_VERSION,
                platform: platformName,
                name: _PLATFORMDATA[platformName]['platform_nicename'],
                url: _PLATFORMDATA[platformName]['downloadurl_prefix'] + urljoin(options['baseUrl'], grunt.ENV.APP_VERSION, _PLATFORMDATA[platformName]['platform_basename'], _PLATFORMDATA[platformName]['downloadurl_file']),
                icon: iconFile
            };
            return platform;
        });


        /**
         * GENERATE PAGE
         */

        // Status
        nemo.log('Generating Page for: ' + binaryQueue.join(', '), '', _TITLE);

        /**
         * ALL PLATFORMS
         */
        // Default Folders
        var pageIndexFile = path.join(outputDirectory, path.basename(pageIndexTemplate)),
            pageAssetsDirectory = path.join(outputDirectory, path.basename(pageAssetsTemplate));

        /** Grunt Configuration */
        grunt.config.merge({
            template: {
                'page-index': {
                    options: {
                        data: {
                            'projectName': pageTitle,
                            'binaryResults': applicationList,
                            'dateTime': '<%= grunt.template.today("yyyy-mm-dd") %>',
                            'manifestConfiguration': options
                        }
                    },
                    src: pageIndexTemplate,
                    dest: pageIndexFile,
                    nonull: true
                }
            },

            copy: {
                'page-assets': {
                    cwd: pageAssetsTemplate,
                    dest: pageAssetsDirectory,
                    src: ['**/*'],
                    expand: true,
                    nonull: true
                }
            }
        });

        // Copy Page Assets
        grunt.task.run(['copy:page-assets']);

        // Page Template
        grunt.task.run(['template:page-index']);

        /**
         * IOS
         */
        if (_.includes(platformList, 'ios')) {
            grunt.task.run(['nemo-page-wireless-manifest']);
        }

    });

    grunt.registerTask('nemo-page-wireless-manifest', 'Page (Wireless Manifest)', function() {

        /** @external */
        var path = require('path'),
            nemo = require('../lib/index.js'),
            _ = require('lodash'),
            urljoin = require('url-join'),
            replaceExt = require('replace-ext');

        /** @constant */
        var _TITLE = 'Component: Page (Wireless Manifest)';

        /** @default */
        var _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];


        // Environment
        var options = grunt.config.get('nemo')['options']['page'],
            outputDirectory = _PLATFORMDATA['output_dir'];

        // Page Template
        var wirelessManifestTemplate = grunt.RESOURCE.HOME(_PLATFORMDATA['ios']['wireless_manifest_file']);

        // Status
        nemo.logger.heading(_TITLE, 'Starting Task');

        /**
         * BUILD LIST OF APPLICATION DOWNLOADS
         */
        // List Executables (from binaries in output path)
        var defaultBinaryExtensionList = ['ipa'],
            binaryQueue = nemo.fs.getFilesDeep(outputDirectory, defaultBinaryExtensionList);

        /**
         * GENERATE WIRELESS MANIFEST
         */
        _.each(binaryQueue, function(binaryFile) {

            // Status
            nemo.log('Generating Page for: ' + binaryQueue.join(', '), '', _TITLE);

            // Fill Template
            grunt.task.run(['template:wireless-manifest']);

            // Defaults
            var applicationIdentifier = nemo.cordovainfo.getAppId(grunt.ENV.APP_FILEPATH),
                applicationVersion = nemo.cordovainfo.getAppVersion(grunt.ENV.APP_FILEPATH),
                applicationName = nemo.cordovainfo.getAppName(grunt.ENV.APP_FILEPATH),
                downloadUrl = urljoin(options['baseUrl'], grunt.ENV.APP_VERSION, binaryFile),
                wirelessManifestFile = path.join(outputDirectory, replaceExt(binaryFile, path.extname(wirelessManifestTemplate)));

            /** Grunt Configuration */
            grunt.config.merge({
                template: {
                    'wireless-manifest': {
                        options: {
                            data: {
                                'applicationIdentifier': applicationIdentifier,
                                'applicationVersion': applicationVersion,
                                'applicationName': applicationName,
                                'downloadUrl': downloadUrl
                            }
                        },
                        src: wirelessManifestTemplate,
                        dest: wirelessManifestFile,
                        nonull: true
                    }
                }
            });
        });
    });
};
