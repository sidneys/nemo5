'use strict';

/**
 * @summary FTP transfer
 * @author Sidney Bofah
 * @created 2015-02-20
 */
module.exports = function(grunt) {

    /** Validate: Configuration structure */
    grunt.registerTask('nemo-deploy', 'NEMO FTP Uploader', function() {

        /** @constant */
        var _TITLE = 'Component: Deploy',
            _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];

        /** @external */
        var path = require('path'),
            nemo = require('../lib/index.js');

        // Options
        var options = grunt.config.get('nemo.options.deploy'),
            urljoin = require('url-join');

        /** Status Message */
        nemo.logger.heading(_TITLE, 'Starting Task');

        // FTP URL
        var ftpUrlComponents = options[0]['ftpUrl'].split('/'),
            ftpHostname = ftpUrlComponents[0],
            ftpBaseFolder = ftpUrlComponents.slice(1).join('/'),
            ftpFolder = urljoin(ftpBaseFolder);

        // FTP Credentials
        var deployPassword = options[0]['ftpPassword'],
            deployUsername = options[0]['ftpUser'];

        var outputDirectory = _PLATFORMDATA['output_dir'],
            uploadDirectory = path.dirname(outputDirectory);

        /**
         * List files to be uploaded
         */
        nemo.logger.log('Deploying: ' + outputDirectory);
        nemo.logger.log('To' + ' ' + ftpFolder + ' ' + 'at' + ' ' + ftpHostname);

        /**
         * Main
         */
        grunt.task.run(['ftp-deploy']);

        /**
         * Peer Task Configuration
         */
        grunt.config.merge({
            'ftp-deploy': {
                build: {
                    forceVerbose: true,
                    dest: ftpFolder,
                    src: uploadDirectory,
                    exclusions: ['path/to/source/folder/**/.DS_Store', 'path/to/source/folder/**/Thumbs.db', 'path/to/dist/tmp'],
                    auth: {
                        host: ftpHostname,
                        port: 21,
                        username: deployUsername,
                        password: deployPassword
                    }
                }
            }
        });
    });
};
