'use strict';

/**
 * @summary Build, codesign and package applications with file-based credentials.
 * @author Sidney Bofah
 * @created 2015-02-01
 */
module.exports = function(grunt) {

    grunt.registerTask('nemo-build', 'Component: Build', function() {

        /** @external */
        var path = require('path'),
            nemo = require('../lib/index.js'),
            _ = require('lodash'),
            renameKeys = require('deep-rename-keys');

        /** @constant */
        var _TITLE = 'Component: Build';

        /** @default */
        var _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];


        // Environment
        var options = grunt.config.get('nemo')['options'];

        // Task Queue
        var platformQueue = Object.keys(options['build']),
            buildQueue = options['build'];

        // Status
        nemo.logger.heading(_TITLE, 'Starting Task');


        /**
         * ITERATE PLATFORMS
         */
        _.each(platformQueue, function(platformName) {

            /**
             * ITERATE BUILDS
             */
            _.each(buildQueue[platformName], function(target, index) {

                /**
                 * PREPARATION
                 */
                // Status
                nemo.log('Build Target: ' + (index + 1) + ' of ' + buildQueue[platformName].length, '', _TITLE);

                // Build list of required files
                var requiredFiles = grunt.RESOURCE.APP(_.filter(target, function(value, key) {
                    return _.endsWith(key, 'File');
                }));

                // Error: Print missing files
                if (nemo.fs.validateFileList(requiredFiles).error) {
                    nemo.logger.errorList(nemo.fs.validateFileList(requiredFiles).lost, 'Missing files for build process target.', platformName);
                    nemo.logger.fail();
                }


                /**
                 * IOS
                 */
                if (_.startsWith(platformName, 'ios')) {

                    // Codesigning
                    // Get Settings
                    var certificatePasswordList = grunt.option('certificatePassword') || [],
                        certificateFile = grunt.RESOURCE.APP(target['certificateFile']),
                        profileFile = grunt.RESOURCE.APP(target['profileFile']),
                        certificatePassword = certificatePasswordList[index] || target['certificatePassword'];

                    // Install
                    //var certificateInstallation = nemo.platform.ios.installCertificate(certificateFile, certificatePassword, false),
                    //    profileInstallation = nemo.platform.ios.installProfile(profileFile);

                    // Errorhandling for Codesigning files
                    //if (!certificateInstallation) {
                    //    nemo.logger.error('Could not install iOS Codesigning Identity. Please check your "certificatePassword" setting: ' + certificatePassword, certificateFile);
                    //    nemo.logger.fail();
                    //}
                    //if (!profileInstallation) {
                    //    nemo.logger.error('Could not install iOS Mobile Device Provisioning Profile.', profileFile);
                    //    nemo.logger.fail();
                    //}

                    // Parse
                    var certificateName = nemo.fs.getCertificate(certificateFile, certificatePassword)['subject']['commonName'],
                        profileId = nemo.fs.getProfile(profileFile)['UUID'];


                    // Devicetype
                    var iosDevicetype = nemo.platform.ios.getDevicetype(options['app']['devicetypes']['ios']);

                    /** Grunt Tasks */
                    grunt.config.merge({
                        template: {
                            'buildconfig-ios': {
                                options: {
                                    data: {
                                        'iosCertificateName': certificateName,
                                        'iosProfileId': profileId,
                                        'iosDevicetype': iosDevicetype
                                    }
                                }
                            }
                        }
                    });
                }


                /**
                 * ANDROID
                 */
                if (_.startsWith(platformName, 'android')) {

                    // Build Tool
                    process.env['ANDROID_BUILD'] = 'gradle';

                    // Split Architectures
                    var splitBinaries = target['splitBinaries'];

                    process.env['BUILD_MULTIPLE_APKS'] = process.env['BUILD_MULTIPLE_ARCHS'] = splitBinaries;

                    // Codesigning
                    // Get Settings (CLI)
                    var keystorePasswordList = grunt.option('keystorePassword') || [];
                    // Get Settings
                    var keystoreFile = grunt.RESOURCE.APP(target['keystoreFile']),
                        keystorePassword = keystorePasswordList[index] || target['keystorePassword'];

                    // Parse
                    var keystoreAlias = nemo.fs.getKeystore(keystoreFile, keystorePassword)['alias'];

                    // Errorhandling for Codesigning files
                    if (!keystoreAlias) {
                        nemo.logger.error('Could not open Android Keystore. Please check your "keystorePassword" setting: ' + keystorePassword, keystoreFile);
                        nemo.logger.fail();
                    }

                    /** Grunt Task Configuration */
                    grunt.config.merge({
                        template: {
                            'buildconfig-android': {
                                options: {
                                    data: {
                                        'androidKeystoreAlias': keystoreAlias,
                                        'androidKeystorePassword': keystorePassword,
                                        'androidKeystoreFile': keystoreFile,
                                        'androidSplitBinaries': splitBinaries
                                    }
                                }
                            }
                        }
                    });
                }


                /**
                 * ALL PLATFORMS
                 */
                // Default Folders
                var buildDirectory = grunt.RESOURCE.APP(_PLATFORMDATA[platformName]['build_dir']),
                    buildconfigTemplate = grunt.RESOURCE.HOME(_PLATFORMDATA[platformName]['buildconfig_file_template']),
                    buildconfigFileName = path.basename(buildconfigTemplate),
                    buildconfigDirectory = grunt.RESOURCE.APP(path.join(_PLATFORMDATA[platformName]['buildconfig_dir']));


                // SDK
                var minimumSdk = options['app']['platforms'][platformName];
                var binaryBasename = _PLATFORMDATA[platformName]['application_file_name'],
                    binaryPackaged = _PLATFORMDATA[platformName]['application_file_packaged'],
                    iconFile = _PLATFORMDATA['page_icon_file'],
                    outputDirectory = path.join(_PLATFORMDATA['output_dir'], platformName);

                /** Common Grunt Configuration */
                var gruntConfigDefault = {
                    template: {
                        'buildconfig-Platform': {
                            options: {
                                data: {
                                    'binaryBasename': binaryBasename,
                                    'buildconfigDirectory': buildconfigDirectory,
                                    'buildDirectory': buildDirectory,
                                    'minimumSdk': minimumSdk
                                }
                            },
                            src: buildconfigTemplate,
                            dest: buildconfigDirectory,
                            expand: true,
                            flatten: true,
                            nonull: true

                        }

                    },

                    clean: {
                        'binary-Platform': {
                            src: binaryBasename
                        },
                        'buildconfig-Platform': {
                            src: buildconfigFileName,
                            expand: true
                        },
                        options: {
                            force: true
                        }
                    },

                    copy: {
                        'binary-Platform': {
                            cwd: buildDirectory,
                            src: binaryPackaged,
                            dest: outputDirectory,
                            nonull: false,
                            expand: true,
                            flatten: true
                        },
                        'icon-Platform': {
                            src: grunt.RESOURCE.APP(_PLATFORMDATA[platformName]['icon_file']),
                            dest: path.join(outputDirectory, iconFile),
                            nonull: true
                        }
                    }
                };

                var gruntConfig = renameKeys(gruntConfigDefault, function(key) {
                    return key.replace('Platform', platformName);
                });

                grunt.config.merge(gruntConfig);

                // Clean Output Folder
                grunt.task.run(['clean:binary-' + platformName, 'clean:buildconfig-' + platformName]);

                // Copy Build Configuration Files
                grunt.task.run(['template:buildconfig-' + platformName]);

                // Trigger Build
                grunt.task.run(['nemo-build-run:' + platformName]);

                // Package Binaries
                grunt.task.run(['nemo-build-package:' + platformName]);

                // Copy Binaries to Output Folder
                grunt.task.run(['copy:binary-' + platformName]);

                // Copy Icons to Output Folder
                grunt.task.run(['copy:icon-' + platformName]);
            });
        });
    });


    grunt.registerTask('nemo-build-run', 'Component: Build (Execute)', function(platformName) {

        /** @external */
        var nemo = require('../lib/index.js');


        /** Task Target */
        var taskTarget = platformName;

        // Init
        var buildArgs = ['build', taskTarget, '--device', '--release'];


        /**
         * Execute Build via Cordova (using --release)
         */
        nemo.cordovafeature.execute(buildArgs);
    });


    grunt.registerTask('nemo-build-package', 'Component: Build (Package)', function(platformName) {

        /** @external */
        var path = require('path'),
            _ = require('lodash'),
            nemo = require('../lib/index.js');

        /** @constant */
        var _TITLE = 'Component: Build (Package)';

        /** @default */
        var _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];


        /** Task Target */
        var taskTarget = platformName;

        // Status
        nemo.logger.heading(_TITLE, taskTarget);


        /** Dynamic Configuration */
        var binaryUnpackaged = path.join(grunt.RESOURCE.APP(_PLATFORMDATA[taskTarget]['build_dir']), nemo.util.pickArray(_PLATFORMDATA[taskTarget]['application_file_unpackaged']));


        /**
         * IOS
         */
        if (_.startsWith(taskTarget, 'ios')) {
            nemo.platform.ios.packageApplication(binaryUnpackaged);
        }

        /**
         * ANDROID
         */
        if (_.startsWith(taskTarget, 'android')) {
            nemo.platform.android.packageApplication(binaryUnpackaged);
        }

    });
};
