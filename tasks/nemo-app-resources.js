'use strict';

/**
 * @summary Copies Icons & Splashscreens to Cordova platforms. Based on generator-ionic hook.
 * @author Sidney Bofah
 * @created 2015-05-15
 */
module.exports = function(grunt) {

    grunt.registerTask('nemo-app-resources', function() {

        /** @constant */
        var _TITLE = 'Cordova Feature Management: Resources (Icons & Splashscreens)',
            _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];

        /** @default */
        var resourcesDirectory = _PLATFORMDATA['resources_dir'];

        /** @external */
        var nemo = require('../lib/index.js'),
            path = require('path'),
            _ = require('lodash'),
            ncp = require('ncp'),
            mkdirp = require('mkdirp'),
            glob = require('glob'),
            Orchestrator = require('orchestrator');

        // Queue
        var taskQueue = nemo.cordovainfo.getPlatformsInstalled(grunt.ENV.APP_FILEPATH);

        /** Status Message */
        nemo.logger.heading(_TITLE, 'Starting Task');

        // Helper function for file copying that ensures directory existence.
        function copyResource(src, dest, ncpOpts, callback) {
            var orchestrator = new Orchestrator();
            var parts = dest.split(path.sep);
            var fileName = parts.pop();
            var destDir = parts.join(path.sep);
            var destFile = path.resolve(destDir, fileName);
            orchestrator.add('ensureDir', function(done) {
                mkdirp(destDir, function(err) {
                    done(err);
                });
            });
            orchestrator.add('copyFile', ['ensureDir'], function(done) {
                ncp(src, destFile, ncpOpts, function(err) {
                    done(err);
                });
            });
            orchestrator.start('copyFile', function(err) {
                callback(err);
            });
        }

        /**
         * ITERATE PLATFORMS
         */
        _.each(taskQueue, function(platformName) {

            // Status
            nemo.logger.logData('Copying ' + _PLATFORMDATA[platformName]['platform_nicename'] + ' Icons & Splashscreens.');

            // Orchestrator
            var resourceDirectory = grunt.RESOURCE.APP(_PLATFORMDATA[platformName]['resources_dir']);
            glob(resourceDirectory + '/**/*.png', function(err, files) {
                _.each(files, function(cordovaFile) {
                    var orchestrator = new Orchestrator();
                    var parts = cordovaFile.split('/');
                    var fileName = parts.pop();
                    var localDir = path.resolve(resourcesDirectory, platformName, _.last(parts));
                    var localFile = path.resolve(localDir, fileName);

                    orchestrator.add('copyFromCordova', function(done) {
                        copyResource(cordovaFile, localFile, { clobber: false }, function(err) {
                            done(err);
                        });
                    });
                    orchestrator.add('copyToCordova', ['copyFromCordova'], function(done) {
                        copyResource(localFile, cordovaFile, { clobber: true }, function(err) {
                            done(err);
                        });
                    });
                    orchestrator.start('copyToCordova', function(err) {
                        if (err) { console.error(err); }
                    });
                });

            });
        });

        // Status
        nemo.logger.logData('Copying Icons & Splashscreens complete.');
    });

};
