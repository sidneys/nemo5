'use strict';

/**
 * @summary Manages core Cordova features (Platforms, Plugins, Webviews) across platforms, heavily leaning on the toolbelt module.
 * @author Sidney Bofah
 * @created 2015-02-01
 */
module.exports = function(grunt) {

    /**
     * Wrappers for Features
     */
    grunt.registerTask('nemo-app-platforms', function() { grunt.task.run(['nemo-app-feature:platforms']); });
    grunt.registerTask('nemo-app-plugins', function() { grunt.task.run(['nemo-app-feature:plugins']); });
    grunt.registerTask('nemo-app-webviews', function() { grunt.task.run(['nemo-app-feature:webviews']); });

    /**
     * Main Task
     */
    grunt.registerTask('nemo-app-feature', 'NEMO Cordova Feature Manager', function(featureName) {

        /** @external */
        var nemo = require('../lib/index.js'),
            _ = require('lodash');

        /** @constant */
        var _TITLE = 'Cordova Feature Management: ' + _.startCase(featureName);

        // Queue
        var featureQueue = grunt.config.get('nemo')['options']['app'][featureName],
            projectPath = grunt.ENV.APP_FILEPATH;

        /** Status Message */
        nemo.logger.heading(_TITLE, 'Starting Task');


        /**
         * Configuration
         */
        /** Handle Webview configuration */
        if (featureName === 'webviews') {
            featureQueue = _.values(grunt.config.get('nemo')['options']['app'][featureName], function(value) {
                return value;
            });
            featureName = 'plugins';
        }

        /** Prepare workspace by removing all feature instances. */
        //nemo.CordovaInfo.deleteComponent(projectPath, featureName);
         console.log(projectPath, featureName, featureQueue, grunt.config.get('nemo')['options']['app'])
        /** Add features to project. */
        nemo.cordovafeature.removeFeature(projectPath, featureName, featureQueue);
        nemo.cordovafeature.installFeature(projectPath, featureName, featureQueue);
    });


    /**
     * App
     */
    grunt.registerTask('nemo-app', ['nemo-app-plugins', 'nemo-app-platforms', 'nemo-app-webviews', 'nemo-app-resources', 'nemo-app-orientation']);

};
