'use strict';

/**
 * @summary Manages platform-specific setting of portrait/landscape view modes.
 * @author Sidney Bofah
 * @created 2015-02-08
 */
module.exports = function(grunt) {

    /**
     * orientation: {
     *   handset: [ "landscape" ],
     *   tablet: [ "landscape", "portrait" ]
     * };
     */
    grunt.registerTask('nemo-app-orientation', function() {

        /** @constant */
        var _TITLE = 'Component: Orientation',
            _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];

        /** @external */
        var path = require('path'),
            nemo = require('../lib/index.js'),
            _ = require('lodash');

        // Load Configuration
        var options = grunt.config.get('nemo')['options']['app']['orientation'];

        // Task Queue(s)
        var platformList = nemo.cordovainfo.getPlatformsInstalled(grunt.ENV.APP_FILEPATH),
            devicetypeList = Object.keys(options);

        // Status Message
        nemo.logger.heading(_TITLE, 'Starting Task');


        /**
         * ITERATE PLATFORMS
         */
        _.each(platformList, function(platformName) {

            // Status Message
            nemo.logger.heading(_PLATFORMDATA[platformName]['platform_nicename']);

            // Status
            nemo.logger.logData('Resetting supported device orientations.');

            /**
             * IOS
             */
            if (_.startsWith(platformName, 'ios')) {
                nemo.platform.ios.resetOrientations(path.join(grunt.ENV.APP_FILEPATH, 'platforms', platformName), nemo.cordovainfo.getAppName(grunt.ENV.APP_FILEPATH));
            }

            /**
             * ITERATE DEVICETYPES
             */
            _.each(devicetypeList, function(devicetypeName) {

                /**
                 * ITERATE ORIENTATIONS
                 */
                _.each(options[devicetypeName], function(orientationName) {

                    // Status
                    nemo.logger.logData('Adding ' + _.startCase(orientationName) + ' support for ' + _.startCase(devicetypeName) + 's.');

                    /**
                     * IOS
                     */
                    if (_.startsWith(platformName, 'ios')) {

                        // Add Device / Orientation support
                        nemo.platform.ios.addOrientation(path.join(grunt.ENV.APP_FILEPATH, 'platforms', 'ios'), devicetypeName, orientationName, nemo.cordovainfo.getAppName(grunt.ENV.APP_FILEPATH));
                    }
                });

            });

        });

        // Status
        nemo.logger.logData('Orientation configuration complete.');
    });
};
