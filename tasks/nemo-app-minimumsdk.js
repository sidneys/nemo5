'use strict';

/**
 * @summary Target minimum system versions per platform (e.g. iOS 8.1, Android API Level 19)
 * @author Sidney Bofah
 * @created 2015-02-08
 */
module.exports = function(grunt) {

    grunt.registerTask('nemo-app-minimumsdk', 'NEMO Adjust minimum SDK', function(target) {
//
//        /**
//         * @constant
//         */
//        var _TITLE = 'Cordova Minimum SDK';
//
//        // Modules
//        var nemo = require('./toolbelt.js'),
//            xml = require('cordova-lib/src/util/xml-helpers'),
//            path = require('path'),
//            _ = require('lodash'),
//       fs = require('fs'),
//            et = require('elementtree');
//
//        // Environment
//        var taskName = grunt.task.current.name,
//            options = grunt.config.get('nemo.options')['app']['platforms'];
//
//        // Queue
//        var targetQueue = options;
//
//        // Init
//        var _PLATFORMDATA = grunt.config.get('_platformData');
//
//        var configFileIos = path.join(_PLATFORMDATA['ios']['platform_dir'], grunt.config.get('pkg.name') + '.xcodeproj', grunt.config.get('pkg.name') + '.pbxproj'),
//            configFileAndroid = path.join(_PLATFORMDATA['android']['platform_dir'], 'AndroidManifest.xml');
//
//        var configHandleAndroid = xml.parseElementtreeSync(configFileAndroid);
//
//        /** Status Message **/
//        nemo.Logger.log('Starting task.', null, _TITLE);
//
//        /**
//         * Configuration
//         */
//        /** Check if Configuration exists */
//        if (nemo.util.isEmpty(targetQueue)) {
//            nemo.Logger.log('Configuration for this task is missing.', target, _TITLE + ':' + taskName);
//        }
//
//        /**
//         * Main
//         */
//        var setupSDK = function(platform) {
//
//            /**
//             * iOS
//             */
//            if (platform.indexOf('ios') !== -1) {
//
//                grunt.task.run(['replace:minsdk-ios:' + options[platform]]);
//            }
//
//            /**
//             * Android
//             */
//            if (platform.indexOf('android') !== -1) {
//
//                var usesSdk;
//                usesSdk = configHandleAndroid.getroot().find('./uses-sdk');
//
//            _.each(['minSdkVersion', 'maxSdkVersion', 'targetSdkVersion'],function(sdkPrefName) {
//                        var sdkPrefValue = sdkPrefName;
//                        if (!sdkPrefValue) {
//                            return;
//                        }
//                        if (!usesSdk) {
//                            usesSdk = new et.Element('uses-sdk');
//                            configHandleAndroid.getroot().append(usesSdk);
//                        }
//                        usesSdk.attrib['android:' + sdkPrefName] = sdkPrefValue;
//                    });
//
//                fs.writeFileSync(configFileAndroid, configHandleAndroid.write({indent: 4}), 'utf-8');
//            }
//            nemo.Logger.log('Setting minimum SDK for platform.', _PLATFORMDATA[platform]['title'], _TITLE + ':' + 'SDK ' + options[platform]);
//        };
//
//
//        /**
//         * Loop through all platforms and apply orientation settings to platforms
//         */
//        _.keys(targetQueue, function(item) {
//           var platform = item.split('@')[0];
//          //  console.log(targetQueue)
//            setupSDK(platform);
//        });
//
//
//        /**
//         * Configuration
//         * Peer Tasks
//         */
//        grunt.config.merge({
//            replace: {
//                'minsdk-ios': {
//                    options: {
//                        patterns: [
//                            {
//                                match: /(IPHONEOS_DEPLOYMENT_TARGET)( )(=)( )(\d+)(\.)(\d+)/g,
//                                replacement: '<%= \'IPHONEOS_DEPLOYMENT_TARGET = \' + grunt.task.this.args %>'
//                            }
//                        ]
//                    },
//                    files: [
//                        {src: ['platforms/ios/<%= pkg._nemo.project.name %>.xcodeproj/project.pbxproj'], dest: 'platforms/ios/<%= pkg._nemo.project.name %>.xcodeproj/project.pbxproj'}
//                    ]
//                }
//            }
//        });
    });

};
