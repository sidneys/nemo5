'use strict';

/**
 * @summary Shows Help / Task list
 * @author Sidney Bofah
 * @created 2015-02-10
 */
module.exports = function(grunt) {
    grunt.registerTask('nemo-help', function() {

        /** @external */
        var nemo = require('../../lib/index.js'),
            chalk = require('chalk'),
            runonymous = require('runonymous-grunt'),
            br = require('os').EOL;

        /** Status Message */
        runonymous(grunt);
        process.stdout.write(br);
        process.stdout.write(chalk.styles.dim.open + chalk.styles.green.open + nemo.terminal.wrapLine('▀▀▀▀▀', '▀▀▀▀▀') + chalk.styles.green.close + chalk.styles.dim.close);
        process.stdout.write(nemo.terminal.wrapLine(chalk.green('To create a new build configuration, run ') + chalk.green.bold('SETUP') + chalk.green('.')));
        process.stdout.write(br);
        process.stdout.write(nemo.terminal.wrapLine(chalk.green('To run all components in sequence, run ') + chalk.green.bold('RELEASE') + chalk.green('.')));
        process.stdout.write(br);
        process.stdout.write(nemo.terminal.wrapLine(chalk.green('To run individual build components, use ') + chalk.green.bold('APP, PAGE, BUILD') + chalk.green(' or ') + chalk.green.bold('DEPLOY') + chalk.green('.')));
        process.stdout.write(br);
        process.stdout.write(chalk.styles.dim.open + chalk.styles.green.open + nemo.terminal.wrapLine('▀▀▀▀▀', '▀▀▀▀▀') + chalk.styles.green.close + chalk.styles.dim.close);

        /**
         * Main
         */
        grunt.task.run('availabletasks:tasks');
        grunt.task.run(function() {
            process.stdout.write(br + br + br);
        });
        grunt.task.run('prompt:tasks');

        /**
         * Peer Task Configuration
         */
        grunt.config.merge({
            'prompt': {
                'tasks': {
                    options: {
                        questions: [
                            {
                                config: 'prompt.task',
                                default: null,
                                message: 'Please select a task to continue.',
                                type: 'list',
                                choices: [
                                    { name: 'Setup', value: 'setup' },
                                    { name: 'App', value: 'app' },
                                    { name: 'Build', value: 'build' },
                                    { name: 'Page', value: 'page' },
                                    { name: 'Deploy', value: 'deploy' },
                                    { name: 'Release', value: 'release' },
                                    '---',
                                    { name: 'Exit', value: false }

                                ]
                            }
                        ],
                        then: function(results) {
                            var selectedTask = results['prompt.task'];

                            if (selectedTask) {
                                // Enqueue chosen task
                                grunt.task.clearQueue();
                                grunt.task.run('nemo-setup');
                                grunt.task.run(selectedTask);
                            }
                        }
                    }
                }
            },
            'availabletasks': {
                tasks: {
                    options: {
                        filter: 'include',
                        sort: ['setup', 'app', 'build', 'page', 'deploy', 'release'],
                        tasks: ['setup', 'app', 'build', 'page', 'deploy', 'release'],
                        descriptions: {
                            'setup': 'Build Manifest Setup.',
                            'app': 'Set-up local Cordova project environment according to the configuration.',
                            'build': 'Build and codesign applications for all platforms and multiple build targets.',
                            'page': 'Generate a custom-facing webpage for over-the-air of app application builds.',
                            'deploy': 'Deploy applications to a webserver via FTP.',
                            'release': 'Runs all tasks in sequence'
                        },
                        groups: {
                            'Configuration': ['setup'],
                            'Components': ['app', 'build', 'page', 'deploy'],
                            'Multi': ['release']
                        },
                        reporter: function(options) {
                            var meta = options.meta,
                                task = options.currentTask;

                            if (meta.header) {
                                process.stdout.write(br);
                                process.stdout.write(br);
                                process.stdout.write(nemo.terminal.wrapLine(chalk.inverse(task.group.toUpperCase()), '_'));
                            }

                            process.stdout.write(nemo.terminal.wrapColumns(chalk.bold.cyan(task.name), chalk.cyan(task.info), '.'));

                        }
                    }
                }
            }
        });
    });

};
