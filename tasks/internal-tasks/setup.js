'use strict';

/**
 * @summary Creates & Validates JSON Configuration (via JSON Schema) and baseline environment.
 * @author Sidney Bofah
 * @created 2015-02-10
 */
module.exports = function(grunt) {

    grunt.registerTask('nemo-setup', function() {

        /** @external */
        var nemo = require('../../lib/index.js');

        /** @constant */
        var _TITLE = 'Component: Setup';

        // Queue Setup steps
        grunt.task.run(['nemo-config-read']);
        grunt.task.run(['nemo-config-apply']);
        grunt.task.run(['nemo-config-save']);


        /** Status Message */
        nemo.logger.heading(_TITLE, 'Starting Task');
    });

    grunt.registerTask('nemo-config-read', function() {

        /** @constant */
        var _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'],
            _CONFIGURATION_FILE_SCHEMA = _PLATFORMDATA['manifest'];

        /** @default */
        var _configurationObjectBufferKeypath = ['nemo', 'optionsBuffer'];

        /** @external */
        var nemo = require('../../lib/index.js'),
            _ = require('lodash');


        // Filesystem / Resolved
        var configurationFileSchema = grunt.RESOURCE.HOME(_CONFIGURATION_FILE_SCHEMA),
            configurationFile = grunt.RESOURCE.APP(_PLATFORMDATA['configuration']);

        /**
         * Main
         */
        // Read Configuration, validate against a schema to return new configuration object
        var configData = nemo.schema.validateFile(configurationFile, configurationFileSchema);

        // Invalid Configuration Object:
        // Prompt user
        if (!_.isObject(configData)) {
            if (configData === false) {
                grunt.task.run(['nemo-config-create']);
            }
        }

        // Valid Configuration Object:
        // Push configuration object to options buffer
        if (_.isObject(configData)) {
            nemo.logger.log('Build manifest passed validation.', nemo.util.beautifyPath(configurationFile));
            grunt.config.set(_configurationObjectBufferKeypath, configData);
        }
    });

    grunt.registerTask('nemo-config-create', function() {

        /** @constant */
        var _TITLE = 'Component: Setup',
            _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'];

        /** @default */
        var _CONFIGURATION_FILE_SCHEMA = _PLATFORMDATA['manifest'],
            _configurationObjectKeypath = ['nemo', 'options'],
            _configurationObjectBufferKeypath = ['nemo', 'optionsBuffer'];

        /** @external */
        var nemo = require('../../lib/index.js'),
            _ = require('lodash');


        // Get components
        var componentQueue = _.filter(nemo.util.compact(grunt.task._queue.map(function(item) {
            return item.task;
        })).map(function(item) {
            return item['name'];
        }).map(function(item) {
            return item.substring(item.indexOf('-') + 1);
        }), function(n) {
            return !_.startsWith(n, 'config');
        });

        // Filesystem / Resolved
        var configurationSchemaFile = grunt.RESOURCE.HOME(_CONFIGURATION_FILE_SCHEMA),
            configurationFile = grunt.RESOURCE.APP(_PLATFORMDATA['configuration']);

        // Settings / Resolved
        var configurationObject = grunt.config.get(_configurationObjectKeypath) || {},
            configurationSchema = nemo.fs.getJSON(configurationSchemaFile),
            configurationDefault = nemo.schema.generateTemplate(configurationSchemaFile),
            configurationObjectBuffer = grunt.config.get(_configurationObjectBufferKeypath);

        /**
         * @description Add Keypath-based properties to options buffer object.
         * @param keyPath {String} Configuration property to add
         * @return {String|Boolean} Keypath-Based options buffer (Closure)
         */
        var addToOptionbuffer = function(keyPath) {

            var optionBufferKeypathBase = _configurationObjectBufferKeypath.join('.'),
                propertyKeypath = keyPath;

            return optionBufferKeypathBase + '.' + propertyKeypath;
        };

        /**
         * Main
         */
        nemo.logger.log('A valid configuration manifest could not be found.', configurationFile);

        // Enqueue startup prompt task
        grunt.task.run('prompt:startup');


        // Enqueue all component prompts
        _.each(componentQueue, function(name) {
            grunt.task.run('prompt:' + name);
        });

        // Save Configuration
        grunt.task.run('nemo-config-save');

        /**
         * Peer Task Configuration
         */
        grunt.config.merge({
            prompt: {
                'startup': {
                    options: {
                        questions: [
                            {
                                config: 'prompt.startup',
                                default: true,
                                message: 'Please select one of the following setup options:',
                                type: 'list',
                                choices: [
                                    // { name: 'Create via interactive prompt.', value: 'wizard' },
                                    { name: 'Create new Manifest (using default values).', value: 'template' },
                                    '---',
                                    { name: 'Retry', value: 'retry' },
                                    { name: 'Abort', value: 'abort' }
                                ]
                            }
                        ],
                        then: function(results) {

                            var response = results['prompt.startup'];

                            /**
                             *  Choice: Wizard
                             */
                            if (response === 'wizard') {
                                // Status
                                nemo.logger.heading('Starting Configuration Wizard.', _TITLE);
                            }

                            /**
                             *  Choice: Defaults
                             */
                            if (response === 'template') {
                                // Status
                                nemo.logger.log('Creating empty configuration template file.', configurationFile, _TITLE);

                                // Create Configuration Object from Defaults, then push it into options Buffer
                                grunt.config.set(_configurationObjectBufferKeypath, _.defaults(configurationObject, configurationDefault));

                                // Continue
                                grunt.task.clearQueue();
                                grunt.task.run('nemo-config-save');

                            }

                            /**
                             *  Choice: Retry
                             */
                            if (response === 'retry') {
                                // Status
                                nemo.logger.log('Restarting manifest validation.', nemo.util.beautifyPath(configurationFile));

                                // Continue
                                grunt.task.clearQueue();
                                grunt.task.run('nemo-config-read');
                            }

                            /**
                             *  Choice: Abort
                             */
                            if (response === 'abort') {
                                // Status
                                nemo.logger.log('Aborting Configuration process.', nemo.util.beautifyPath(configurationFile));
                                // Continue
                                process.exit(1);
                            }

                        }
                    }
                },
                'app': {
                    options: {
                        questions: [
                            {
                                config: addToOptionbuffer('app.platforms'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.platforms'),
                                type: 'checkbox',
                                choices: [
                                    { name: 'ios', checked: true },
                                    { name: 'android', checked: true }
                                ],
                                getFeatureInstalled: function(values) {
                                    var platformMap = {};
                                    _.each(values, function(platformName) {
                                        platformMap[platformName] = nemo.schema.getDefault(configurationSchema, 'app.platforms')[platformName];
                                    });
                                    return platformMap;
                                },
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                config: addToOptionbuffer('app.platforms.ios'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.platforms.ios'),
                                default: nemo.schema.getDefault(configurationSchema, 'app.platforms.ios'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function(answers) {
                                    return answers[addToOptionbuffer('app.platforms')]['ios'];
                                }
                            },
                            {
                                config: addToOptionbuffer('app.platforms.android'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.platforms.android'),
                                default: nemo.schema.getDefault(configurationSchema, 'app.platforms.android'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function(answers) {
                                    return answers[addToOptionbuffer('app.platforms')]['android'];
                                }
                            },
                            {
                                config: addToOptionbuffer('app.platforms.android'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.platforms.android'),
                                default: nemo.schema.getDefault(configurationSchema, 'app.platforms.android'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function(answers) { return answers['promptHelper.platforms.android']; }
                            },
                            {
                                config: addToOptionbuffer('app.plugins'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.plugins'),
                                type: 'checkbox',
                                choices: [
                                    { name: 'org.apache.cordova.console', checked: true },
                                    { name: 'org.apache.cordova.device', checked: true },
                                    { name: 'com.ionic.keyboard', checked: true },
                                    { name: 'org.apache.cordova.splashscreen', checked: true },
                                    { name: 'org.apache.cordova.statusbar', checked: false }
                                ]
                            },
                            {
                                config: addToOptionbuffer('app.devicetypes.ios'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.platforms.android'),
                                type: 'checkbox',
                                choices: [
                                    { name: 'handset', checked: true },
                                    { name: 'tablet', checked: false }
                                ],
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.ios'); }
                            },
                            {
                                config: addToOptionbuffer('app.devicetypes.android'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.devicetypes.android'),
                                type: 'checkbox',
                                choices: [
                                    { name: 'handset', checked: true },
                                    { name: 'tablet', checked: false }
                                ],
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.android'); }
                            },
                            {
                                config: addToOptionbuffer('app.orientation.ios'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.orientation.ios'),
                                type: 'checkbox',
                                choices: [
                                    { name: 'landscape', checked: true },
                                    { name: 'portrait', checked: false }
                                ],
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.ios'); }
                            },
                            {
                                config: addToOptionbuffer('app.orientation.android'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.orientation.android'),
                                type: 'checkbox',
                                choices: [
                                    { name: 'landscape', checked: true },
                                    { name: 'portrait', checked: false }
                                ],
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.android'); }
                            },
                            {
                                config: addToOptionbuffer('app.webviews.ios'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.webviews.ios'),
                                type: 'list',
                                default: null,
                                choices: [
                                    { name: 'default', value: null },
                                    '---',
                                    { name: 'WKWebView', value: 'wkwebview' }
                                ],
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.ios'); },
                                getFeatureInstalled: function(values) {
                                    if (values.indexOf('default') !== -1) {
                                        return null;
                                    }
                                    return values;
                                }
                            },
                            {
                                config: addToOptionbuffer('app.webviews.android'),
                                message: nemo.schema.getDescription(configurationSchema, 'app.webviews.android'),
                                type: 'list',
                                default: null,
                                choices: [
                                    { name: 'default', value: null },
                                    '---',
                                    { name: 'Crosswalk (Canary)', value: 'crosswalk@14.42.335.0' },
                                    { name: 'Crosswalk (Beta)', value: 'crosswalk@13.42.319.6' },
                                    { name: 'Crosswalk Stable', value: 'crosswalk' }
                                ],
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.android'); },
                                getFeatureInstalled: function(values) {
                                    if (values.indexOf('default') !== -1) {
                                        return null;
                                    }
                                    return values;
                                }
                            }
                        ],
                        then: function() {
                            // Remove redundant configuration
                            nemo.util.compact(grunt.config.data.nemo);

                            // Status
                            nemo.logger.heading('Component setup complete: Cordova Application', _TITLE);
                            nemo.terminal.printObject(grunt.config.get(_configurationObjectBufferKeypath));
                        }
                    }
                },
                'build': {
                    options: {
                        questions: [
                            {
                                config: addToOptionbuffer('build.ios.branchName'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.ios.@first.branchName'),
                                type: 'list',
                                default: nemo.git.getActiveBranch(grunt.ENV.APP_FILEPATH),
                                choices: nemo.git.listBranches(grunt.ENV.APP_FILEPATH),
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() {
                                    return (nemo.git.exists(grunt.ENV.APP_FILEPATH) && nemo.git.listBranches(grunt.ENV.APP_FILEPATH) && addToOptionbuffer('app.platforms.ios'));
                                }
                            },
                            {
                                config: addToOptionbuffer('build.ios.profileFile'),
                                default: nemo.schema.getDefault(configurationSchema, 'build.ios.@first.profileFile'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.ios.@first.profileFile'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.ios'); }
                            },
                            {
                                config: addToOptionbuffer('build.ios.certificateFile'),
                                default: nemo.schema.getDefault(configurationSchema, 'build.ios.@first.certificateFile'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.ios.@first.certificateFile'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.ios'); }
                            },
                            {
                                config: addToOptionbuffer('build.ios.certificatePassword'),
                                default: nemo.schema.getDefault(configurationSchema, 'build.ios.@first.certificatePassword'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.ios.@first.certificatePassword'),
                                type: 'password',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.ios'); }
                            },
                            {
                                config: addToOptionbuffer('build.android.branchName'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.android.@first.branchName'),
                                type: 'list',
                                default: nemo.git.getActiveBranch(grunt.ENV.APP_FILEPATH),
                                choices: nemo.git.listBranches(grunt.ENV.APP_FILEPATH),
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() {
                                    return nemo.git.isInitialized(grunt.ENV.APP_FILEPATH) && nemo.git.listBranches(grunt.ENV.APP_FILEPATH) && addToOptionbuffer('app.platforms.android');
                                }
                            },
                            {
                                config: addToOptionbuffer('build.android.keystoreAlias'),
                                default: nemo.schema.getDefault(configurationSchema, 'build.android.@first.keystoreAlias'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.android.@first.keystoreAlias'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.android'); }
                            },
                            {
                                config: addToOptionbuffer('build.android.keystoreFile'),
                                default: nemo.schema.getDefault(configurationSchema, 'build.android.@first.keystoreFile'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.android.@first.keystoreFile'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.android'); }
                            },
                            {
                                config: addToOptionbuffer('build.android.keystorePassword'),
                                default: nemo.schema.getDefault(configurationSchema, 'build.android.@first.keystorePassword'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.android.@first.keystorePassword'),
                                type: 'password',
                                validate: function(value) { return !_.isEmpty(value); },
                                when: function() { return addToOptionbuffer('app.platforms.android'); }
                            },
                            {
                                config: addToOptionbuffer('build.standalone'),
                                default: nemo.schema.getDefault(configurationSchema, 'build.standalone'),
                                message: nemo.schema.getDescription(configurationSchema, 'build.standalone'),
                                type: 'list',
                                choices: [
                                    { name: 'Yes', value: true },
                                    { name: 'No', value: false }
                                ],
                                validate: function(value) { return !_.isEmpty(value); }
                            }

                        ],
                        then: function() {

                            // Interim workaround until support for multiple targets
                            // TODO: Remove
                            var buildTargetsIos = grunt.config.data.nemo.optionsBuffer.build.ios,
                                buildTargetsAndroid = grunt.config.data.nemo.optionsBuffer.build.android;
                            grunt.config.data.nemo.optionsBuffer.build.ios = [];
                            grunt.config.data.nemo.optionsBuffer.build.android = [];
                            grunt.config.data.nemo.optionsBuffer.build.ios[0] = buildTargetsIos;
                            grunt.config.data.nemo.optionsBuffer.build.android[0] = buildTargetsAndroid;

                            // Status
                            nemo.logger.heading('Component setup complete: Build & Signing', _TITLE);
                            nemo.terminal.printObject(grunt.config.get(_configurationObjectBufferKeypath));
                        }
                    }
                },
                'page': {
                    options: {
                        questions: [
                            {
                                //config: 'setup.page.summary',
                                config: addToOptionbuffer('page.summary'),
                                default: nemo.schema.getDefault(configurationSchema, 'page.summary'),
                                message: nemo.schema.getDescription(configurationSchema, 'page.summary'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                //config: 'setup.page.recipient.title',
                                config: addToOptionbuffer('page.recipient.title'),
                                default: nemo.schema.getDefault(configurationSchema, 'page.recipient.title'),
                                message: nemo.schema.getDescription(configurationSchema, 'page.recipient.title'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                //config: 'setup.page.recipient.website',
                                config: addToOptionbuffer('page.recipient.website'),
                                default: nemo.schema.getDefault(configurationSchema, 'page.recipient.website'),
                                message: nemo.schema.getDescription(configurationSchema, 'page.recipient.website'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                //config: 'setup.page.author.title',
                                config: addToOptionbuffer('page.author.title'),
                                default: nemo.schema.getDefault(configurationSchema, 'page.author.title'),
                                message: nemo.schema.getDescription(configurationSchema, 'page.author.title'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                //config: 'setup.page.author.website',
                                config: addToOptionbuffer('page.author.website'),
                                default: nemo.schema.getDefault(configurationSchema, 'page.author.website'),
                                message: nemo.schema.getDescription(configurationSchema, 'page.author.website'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            }

                        ],
                        then: function() {
                            // Status
                            nemo.logger.heading('Component setup complete: Webpage Contents', _TITLE);
                            nemo.terminal.printObject(grunt.config.get(_configurationObjectBufferKeypath));
                        }
                    }
                },
                'deploy': {
                    options: {
                        questions: [
                            {
                                config: addToOptionbuffer('deploy.title'),
                                default: nemo.schema.getDefault(configurationSchema, 'deploy.@first.title'),
                                message: nemo.schema.getDescription(configurationSchema, 'deploy.@first.title'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                config: addToOptionbuffer('deploy.httpUrl'),
                                default: nemo.schema.getDefault(configurationSchema, 'deploy.@first.httpUrl'),
                                message: nemo.schema.getDescription(configurationSchema, 'deploy.@first.httpUrl'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                config: addToOptionbuffer('deploy.ftpUrl'),
                                default: nemo.schema.getDefault(configurationSchema, 'deploy.@first.ftpUrl'),
                                message: nemo.schema.getDescription(configurationSchema, 'deploy.@first.ftpUrl'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                config: addToOptionbuffer('deploy.ftpUser'),
                                default: nemo.schema.getDefault(configurationSchema, 'deploy.@first.ftpUser'),
                                message: nemo.schema.getDescription(configurationSchema, 'deploy.@first.ftpUser'),
                                type: 'input',
                                validate: function(value) { return !_.isEmpty(value); }
                            },
                            {
                                config: addToOptionbuffer('deploy.ftpPassword'),
                                default: nemo.schema.getDefault(configurationSchema, 'deploy.@first.ftpPassword'),
                                message: nemo.schema.getDescription(configurationSchema, 'deploy.@first.ftpPassword'),
                                type: 'password',
                                validate: function(value) { return !_.isEmpty(value); }
                            }

                        ],
                        then: function() {

                            // Interim workaround until support for multiple targets
                            // TODO: Remove
                            var deployTargets = grunt.config.data.nemo.optionsBuffer.deploy;

                            grunt.config.data.nemo.optionsBuffer.deploy = [];
                            grunt.config.data.nemo.optionsBuffer.deploy[0] = deployTargets;

                            // Status
                            nemo.logger.heading('Component setup complete: Deployment', _TITLE);
                            nemo.terminal.printObject(grunt.config.get(_configurationObjectBufferKeypath));
                        }
                    }
                },
                'save': {
                    options: {
                        questions: [
                            {
                                config: 'promptHelper.save',
                                default: true,
                                message: 'Save new configuration?',
                                type: 'list',
                                choices: [
                                    { name: 'Yes', value: true },
                                    { name: 'No', value: false }
                                ]
                            }
                        ],
                        then: function(results) {

                            var response = results['promptHelper.startup'];

                            /**
                             *  Choice: Abort
                             */
                            if (response === false) {
                                // Status
                                nemo.logger.log('Restarting manifest setup.', null, _TITLE);
                                // Continue
                                grunt.task.run('prompt:startup');
                            }

                            /**
                             *  Choice: Save
                             */
                            if (response === true) {
                                // Create Configuration Object from current options buffer data
                                grunt.config.set(_configurationObjectBufferKeypath, _.defaults(configurationObjectBuffer, configurationDefault));
                                // Status
                                nemo.logger.log('Activated configuration from manifest file.', null, _TITLE);
                            }
                        }
                    }
                }
            }
        });
    });

    grunt.registerTask('nemo-config-save', function() {

        /** @constant */
        var _TITLE = 'Component: Setup';

        /** @default */
        var _PLATFORMDATA = grunt.config.get('nemo')['_CONSTANT'],
            nemo = require('../../lib/index.js'),
            _ = require('lodash');


        /** @default */
        var _configurationObjectBufferKeypath = ['nemo', 'optionsBuffer'],
            _configurationFileName = _PLATFORMDATA['configuration'];

        // Init
        var file = grunt.RESOURCE.APP(_configurationFileName),
            configurationCurrent = nemo.fs.getJSON(file) || {},
            configuration = grunt.config.get(_configurationObjectBufferKeypath),
            saveResult;

        // Merge new values into current Manifest, overwriting existing ones.
        configuration = _.merge(configurationCurrent, configuration);

        // Write options to file
        saveResult = nemo.fs.writeJSON(file, configuration);

        if (saveResult === false) {
            nemo.logger.error('Could not write manifest.', file, _TITLE);
            return false;
        }

        nemo.logger.log('Configuration saved.', file, _TITLE);
    });

    grunt.registerTask('nemo-config-apply', function() {

        /** @constant */
        var _TITLE = 'Build Manifest Activation';

        /** @default */
        var _configurationObjectKeypath = ['nemo', 'options'],
            _configurationObjectBufferKeypath = ['nemo', 'optionsBuffer'];

        /** @external */
        var nemo = require('../../lib/index.js'),
            _ = require('lodash');

        // Settings / Resolved
        var configurationObject = grunt.config.get(_configurationObjectBufferKeypath);

        /**
         * @description Apply configuration to 'nemo.options'
         * @param gruntTarget {Array} Grunt configuration target
         * @param configData {String} nemo5 Configuration Object
         * @return {Boolean|String} Configuration
         */
        var applyConfiguration = function(gruntTarget, configData) {

            // Pre-Check: Configuration Target Property
            if (!_.isArray(gruntTarget)) {
                nemo.logger.error('(Internal error) Configuration Target invalid.', gruntTarget.join(), _TITLE);
                return false;
            }

            // Pre-Check: Configuration Object
            if (!_.isObject(configData)) {
                nemo.logger.error('(Internal error) Configuration Object invalid.', configData, _TITLE);
                return false;
            }

            // Apply Configuration Object to Target
            grunt.config.set(gruntTarget, configData);

            // Test whether Configuration was loaded
            if (_.isObject(grunt.config.get(gruntTarget))) {
                return grunt.config.get(gruntTarget);
            } else {
                nemo.logger.error('(Internal error) Configuration object could no be injected into target.', gruntTarget.join(), _TITLE);
                return false;
            }
        };

        /**
         * Main
         */
        // Apply configuration from optionsBuffer
        var configuration = applyConfiguration(_configurationObjectKeypath, configurationObject);

        if (!configuration) {
           nemo.logger.fail('(Internal error) Configuration object missing.');
        }


        // Ask for Confirmation
        grunt.task.run('prompt:continue');

        nemo.logger.heading('Configuration applied successfully.', _TITLE);
        nemo.terminal.printObject(configuration);

        /**
         * Peer Task Configuration
         */
        grunt.config.merge({
            prompt: {
                'continue': {
                    options: {
                        questions: [
                            {
                                config: 'promptHelper.continue',
                                default: true,
                                message: 'Valid build manifest found. Press enter to continue.',
                                type: 'list',
                                choices: [
                                    { name: 'Continue', value: true },
                                    { name: 'Abort', value: false }
                                ],
                                when: function() { return grunt.config.get('build.standalone') === true; }
                            }
                        ],
                        then: function(results) {

                            var response = results['promptHelper.continue'];

                            /**
                             *  Choice: Abort
                             */
                            if (response === 'abort') {
                                // Status
                                nemo.logger.log('Aborted.', _TITLE);
                                // Continue
                                grunt.task.clearQueue();
                            }
                        }
                    }
                }
            }
        });
    });

};
